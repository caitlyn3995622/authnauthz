using Caitlyn.AuthNAuthZ.Domain.Cqrs.Users;
using Caitlyn.AuthNAuthZ.Infrastructure.Neo4j.Models;
using Caitlyn.AuthNAuthZ.Infrastructure.Neo4j.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;
using Neo4j.Driver;

namespace Caitlyn.AuthNAuthZ.Infrastructure.Neo4j;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddNeo4j(this IServiceCollection serviceCollection, IConfiguration configuration)
    {
        serviceCollection
            .Configure<Neo4jConfiguration>(configuration.GetSection("Neo4j"))
            .AddScoped<IDriver>(sp =>
            {
                var config = sp.GetRequiredService<IOptions<Neo4jConfiguration>>().Value;
                var uri = new Uri(config.Uri);
                return GraphDatabase.Driver(uri, AuthTokens.Basic(config.UserName, config.Password));
            })
            .AddScoped<Neo4jContext>()
            .AddScoped<IUserRepository, UserRepository>()
            .AddScoped<IPasswordHasher<DatabaseUser>, PasswordHasher<DatabaseUser>>();
        
        return serviceCollection;
    }
}