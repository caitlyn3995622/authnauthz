using Caitlyn.AuthNAuthZ.Infrastructure.Neo4j.Models;
using Neo4j.Driver;

namespace Caitlyn.AuthNAuthZ.Infrastructure.Neo4j;

internal class Neo4jContext
{
    private readonly IDriver _driver;

    public Neo4jContext(IDriver driver)
    {
        _driver = driver;
    }

    public async Task CreateNodeAsync(Neo4jNodeBase node)
    {
        var serializedDatabaseUser = node.SerializeAllPropertiesForNeo4j();
        await using var session = _driver.AsyncSession(); 
        await session.ExecuteWriteAsync(async queryRunner =>
        {
            await queryRunner.RunAsync($"CREATE(:{serializedDatabaseUser})");
        });
    }

    public async Task<IRecord?> GetAsync(string match)
    {
        var query = $"MATCH (x:{match}) RETURN x";

        await using var session = _driver.AsyncSession(); 
        return await session.ExecuteReadAsync(async queryRunner =>
        {
            var result = await queryRunner.RunAsync(query);
            var hasRecord = await result.FetchAsync();
            return hasRecord ? result.Current : null;
        });
    }

    public async Task RemoveAsync(string match)
    {
        var query = $"MATCH (x:{match}) DETACH DELETE x";
        await using var session = _driver.AsyncSession(); 
        await session.ExecuteWriteAsync(async queryRunner =>
        {
            await queryRunner.RunAsync(query);
        });
    }

    public async Task<IRecord> UpdateAsync(string match, string set)
    {
        var query = $"MATCH (x:{match}) SET {set} RETURN x";
        await using var session = _driver.AsyncSession(); 
        return await session.ExecuteWriteAsync(async queryRunner =>
        {
            var executionResult =  await queryRunner.RunAsync(query);
            var hasRecord = await executionResult.FetchAsync();
            if (!hasRecord) throw new InvalidOperationException("Update query returned no records");

            return executionResult.Current;
        });
    }
}