namespace Caitlyn.AuthNAuthZ.Infrastructure.Neo4j;

internal class Neo4jConfiguration
{
    public string Uri { get; set; } = string.Empty;
    public string UserName { get; set; } = string.Empty;
    public string Password { get; set; } = string.Empty;
}