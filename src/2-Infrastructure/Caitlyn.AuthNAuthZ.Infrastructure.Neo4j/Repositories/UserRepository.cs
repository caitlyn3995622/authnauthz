using Caitlyn.AuthNAuthZ.Domain.Cqrs.Users;
using Caitlyn.AuthNAuthZ.Domain.Models;
using Caitlyn.AuthNAuthZ.Domain.Models.Errors;
using Caitlyn.AuthNAuthZ.Domain.Models.Users;
using Caitlyn.AuthNAuthZ.Infrastructure.Neo4j.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.FSharp.Core;
using Neo4j.Driver;

namespace Caitlyn.AuthNAuthZ.Infrastructure.Neo4j.Repositories;

internal class UserRepository : IUserRepository
{
    private readonly IPasswordHasher<DatabaseUser> _passwordHasher;
    private readonly Neo4jContext _neo4JContext;

    public UserRepository(IPasswordHasher<DatabaseUser> passwordHasher, Neo4jContext neo4JContext)
    {
        _passwordHasher = passwordHasher;
        _neo4JContext = neo4JContext;
    }

    public async Task<bool> CheckIfUserNameAlreadyInUseAsync(string userName)
    {
        var match = Neo4jNodeBase.SerializeForMatch(EntityType.User, new Dictionary<string, string>()
        {
            { nameof(DatabaseUser.UserName), userName }
        });    
        
        var record  = await _neo4JContext.GetAsync(match);
        return record is not null;
    }

    public async Task<bool> CheckIfEmailAlreadyInUseAsync(string email)
    {
        var match = Neo4jNodeBase.SerializeForMatch(EntityType.User, new Dictionary<string, string>()
        {
            { nameof(DatabaseUser.Email), email }
        });    
        
        var record  = await _neo4JContext.GetAsync(match);
        return record is not null;    
    }

    public async Task<User.UserDto> AddAsync(NewUser.NewUser user)
    {
        var databaseUser = new DatabaseUser(user);
        var passwordHash = _passwordHasher.HashPassword(databaseUser, NewUser.getPassword(user)) ?? throw new InvalidOperationException("Unable to hash password");
        
        databaseUser.PasswordHash = passwordHash;
        await _neo4JContext.CreateNodeAsync(databaseUser);

        var userDto = databaseUser.ToDto();
        return userDto;
    }

    public async Task<FSharpOption<User.UserDto>> GetAsync(Types.Uuid.Uuid uuid)
    {
        var match = Neo4jNodeBase.SerializeForMatch(EntityType.User, new Dictionary<string, string>()
        {
            { nameof(DatabaseUser.Uuid), Types.Uuid.evaluate(uuid).ToString() }
        });

        var record  = await _neo4JContext.GetAsync(match);
        if(record is null) return FSharpOption<User.UserDto>.None;
        var node = (INode)record.Values.First().Value;
        
        var user = new DatabaseUser(node);
        return FSharpOption<User.UserDto>.Some(user.ToDto());
    }

    public async Task<FSharpOption<Errors.DomainErrors>> RemoveAsync(Types.Uuid.Uuid uuid)
    {
        var user = await GetAsync(uuid);
        if(FSharpOption<User.UserDto>.get_IsNone(user)) 
            return FSharpOption<Errors.DomainErrors>.Some(Errors.DomainErrors.NewUserError(Errors.UserErrors.UserNotFound, Errors.ErrorType.NotFound));
        
        var match = Neo4jNodeBase.SerializeForMatch(EntityType.User, new Dictionary<string, string>()
        {
            { nameof(DatabaseUser.Uuid), Types.Uuid.evaluate(uuid).ToString() }
        });

        await _neo4JContext.RemoveAsync(match);

        return FSharpOption<Errors.DomainErrors>.None;
    }

    public async Task<FSharpResult<User.UserDto, Errors.DomainErrors>> EditProfileAsync(Types.Uuid.Uuid uuid, EditUserProfile.EditUserProfile editUserProfile)
    {
        var user = await GetAsync(uuid);
        if(FSharpOption<User.UserDto>.get_IsNone(user)) 
            return FSharpResult<User.UserDto, Errors.DomainErrors>.NewError(Errors.DomainErrors.NewUserError(Errors.UserErrors.UserNotFound, Errors.ErrorType.NotFound));
        
        var match = Neo4jNodeBase.SerializeForMatch(EntityType.User, new Dictionary<string, string>()
        {
            { nameof(DatabaseUser.Uuid), Types.Uuid.evaluate(uuid).ToString() }
        });
        var set = Neo4jNodeBase.SerializeForSet(new Dictionary<string, string>
        {
            { nameof(DatabaseUser.Email), EditUserProfile.evaluateEmail(editUserProfile) }
        });

        var record  = await _neo4JContext.UpdateAsync(match, set);
        var node = (INode)record.Values.First().Value;
        
        var updatedUser = new DatabaseUser(node);
        return FSharpResult<User.UserDto, Errors.DomainErrors>.NewOk(updatedUser.ToDto());
    }
}