namespace Caitlyn.AuthNAuthZ.Infrastructure.Neo4j.Models;

internal interface INeo4jNode
{
    string SerializeAllPropertiesForNeo4j();
}