using Caitlyn.AuthNAuthZ.Domain.Models;
using Caitlyn.AuthNAuthZ.Domain.Models.Users;
using Microsoft.AspNetCore.Identity;
using Neo4j.Driver;
using Neo4j.Driver.Internal.Types;

namespace Caitlyn.AuthNAuthZ.Infrastructure.Neo4j.Models;

internal class DatabaseUser: Neo4jNodeBase
{
    public Guid Uuid { get; set; }
    public string UserName { get; set; } = string.Empty;
    public string PasswordHash { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;

    public DatabaseUser(): base(EntityType.User)
    {
        
    }

    public DatabaseUser(INode neo4jRecord) : base(EntityType.User)
    {
        var values = neo4jRecord.Properties;
        
        ExtractAndExecuteWithMandatoryRecordValue<string>(values, nameof(Uuid), x => Uuid = Guid.Parse(x));
        ExtractAndExecuteWithOptionalRecordValue<string>(values, nameof(UserName), x=> UserName = x);
        ExtractAndExecuteWithOptionalRecordValue<string>(values, nameof(PasswordHash), x=> PasswordHash = x);
        ExtractAndExecuteWithOptionalRecordValue<string>(values, nameof(Email), x=> Email = x);
    }

    public DatabaseUser(NewUser.NewUser user): base(EntityType.User)
    {
        Uuid = Guid.NewGuid();
        UserName = NewUser.getUserName(user);
        Email = NewUser.getEmail(user);
    }

    public User.UserDto ToDto() => new User.UserDto(Uuid, UserName, Email);

    protected override IEnumerable<KeyValuePair<string, string>> GetAllPropertiesToSerialize() => new Dictionary<string, string>
    {
        { nameof(Uuid), Uuid.ToString() },
        { nameof(UserName), UserName},
        {nameof(PasswordHash), PasswordHash},
        {nameof(Email), Email},
    };
}