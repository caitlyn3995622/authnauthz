using System.Text;
using Neo4j.Driver;

namespace Caitlyn.AuthNAuthZ.Infrastructure.Neo4j.Models;

internal abstract class Neo4jNodeBase : INeo4jNode
{
    private readonly EntityType _entityType;
    
    protected Neo4jNodeBase(EntityType entityType)
    {
        _entityType = entityType;
    }

    public string SerializeAllPropertiesForNeo4j() => SerializeForMatch(_entityType, GetAllPropertiesToSerialize());
    
    protected abstract IEnumerable<KeyValuePair<string, string>> GetAllPropertiesToSerialize();

    protected void ExtractAndExecuteWithOptionalRecordValue<TValueType>(IReadOnlyDictionary<string, object> values, string recordName, Action<TValueType> callback)
    {
        if (values.TryGetValue(recordName, out var extractedValue) && extractedValue is TValueType convertedValue)
        {
            callback.Invoke(convertedValue);
        }
    }
    
    protected void ExtractAndExecuteWithMandatoryRecordValue<TValueType>(IReadOnlyDictionary<string, object> values, string recordName, Action<TValueType> callback)
    {
        if (!values.TryGetValue(recordName, out var extractedValue) || extractedValue is not TValueType convertedValue)
            throw new InvalidOperationException($"Neo4j record doesn't have {recordName} property");
        
        callback.Invoke(convertedValue);
    }
    
    public static string SerializeForMatch(EntityType entityType, IEnumerable<KeyValuePair<string, string>> properties)
    {
        entityType.ToString();
        var serializedProperties = GetSerializedPropertiesForMatch(properties);
        
        return $"{entityType} {serializedProperties}";
    }
    
    public static string SerializeForSet(IEnumerable<KeyValuePair<string, string>> properties)
    {
        var stringProperties = properties
            .Select(x => $"x.{x.Key} = '{x.Value}'");

        return string.Join(", ", stringProperties);    }

    private static string GetSerializedPropertiesForMatch(IEnumerable<KeyValuePair<string, string>> properties)
    {
        var stringProperties = properties
            .Select(x => $"{x.Key}: '{x.Value}'");

        var serializedProperties = string.Join(", ", stringProperties);

        return new StringBuilder()
            .Append("{")
            .Append(serializedProperties)
            .Append("}")
            .ToString();
    }

}