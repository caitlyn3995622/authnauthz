namespace Caitlyn.AuthNAuthZ.Presentation.Api;

public class ApiResult<TResult>: ApiResult
{
    public TResult? Result { get; set; }
}

public class ApiResult
{
    public bool IsOk => !Errors.Any();
    public IEnumerable<string> Errors { get; set; } = new List<string>();
}