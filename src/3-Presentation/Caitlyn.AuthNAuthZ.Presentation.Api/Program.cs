using Caitlyn.AuthNAuthZ.CrossCutting.Extensions;
using Caitlyn.AuthNAuthZ.Infrastructure.Neo4j;
using Caitlyn.AuthNAuthZ.Presentation.Api.Extensions;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration
    .SetBasePath(typeof(Program).Assembly.GetDirectoryName())
    .AddJsonFile("appsettings.neo4j.json", optional: true)
    .AddEnvironmentVariables();

// Add services to the container.

Caitlyn.AuthNAuthZ.Domain.Cqrs.ServiceCollectionExtensions.AddCqrs(builder.Services);
Caitlyn.AuthNAuthZ.Domain.Models.ServiceCollectionExtensions.AddErrorMapper(builder.Services)
    .AddEndpointsApiExplorer()
    .AddSwagger()
    .AddVersioning()
    .AddControllerAndConfigureJsonOptions()
    .AddNeo4j(builder.Configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) app.ConfigureSwaggerWithVersioning();

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();


public partial class Program
{
}