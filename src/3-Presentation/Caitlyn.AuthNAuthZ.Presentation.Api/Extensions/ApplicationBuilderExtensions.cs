using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace Caitlyn.AuthNAuthZ.Presentation.Api.Extensions;

internal static class ApplicationBuilderExtensions
{
    public static WebApplication ConfigureSwaggerWithVersioning(this WebApplication app)
    {
        var apiVersionDescriptionProvider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();
        app.UseSwagger();
        app.UseSwaggerUI(options =>
        {
            options.DefaultModelsExpandDepth(-1);
            
            foreach (var description in apiVersionDescriptionProvider.ApiVersionDescriptions.Reverse())
            {
                options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                    description.GroupName.ToUpperInvariant());
            }
        });
        
        return app;
    }
}