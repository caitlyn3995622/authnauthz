using System.Text.Json;
using System.Text.Json.Serialization;
using Caitlyn.AuthNAuthZ.Presentation.Api.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Options;

namespace Caitlyn.AuthNAuthZ.Presentation.Api.Extensions;

internal static class ServiceCollectionExtensions
{
    public static IServiceCollection AddVersioning(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddApiVersioning(opt =>
        {
            opt.DefaultApiVersion = new ApiVersion(1,0);
            opt.AssumeDefaultVersionWhenUnspecified = true;
            opt.ReportApiVersions = true;
            opt.ApiVersionReader = ApiVersionReader.Combine(
                new UrlSegmentApiVersionReader(),
                new HeaderApiVersionReader("x-api-version"));
        });

        serviceCollection.AddVersionedApiExplorer(setup =>
        {
            setup.GroupNameFormat = "'v'VVV";
            setup.SubstituteApiVersionInUrl = true;
        });
        return serviceCollection;
    }

    public static IServiceCollection AddSwagger(this IServiceCollection serviceCollection)
    {
        serviceCollection
            .AddSwaggerGen()
            .ConfigureOptions<ConfigureSwaggerOptions>();

        return serviceCollection;
    }

    public static IServiceCollection AddControllerAndConfigureJsonOptions(this IServiceCollection serviceCollection)
    {
        serviceCollection
            .AddControllers()
            .AddJsonOptions(x =>
            {
                x.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });

        serviceCollection.AddSingleton<JsonSerializerOptions>(sp =>
        {
            var jsonOptions = sp.GetRequiredService<IOptions<JsonOptions>>().Value;
            return jsonOptions.JsonSerializerOptions;
        });

        return serviceCollection;
    }
}