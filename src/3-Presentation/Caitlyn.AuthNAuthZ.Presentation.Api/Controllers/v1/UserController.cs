using System.Net;
using Caitlyn.AuthNAuthZ.Domain.Cqrs.Users;
using Caitlyn.AuthNAuthZ.Domain.Models.Errors;
using Caitlyn.AuthNAuthZ.Domain.Models.Users;
using Microsoft.AspNetCore.Mvc;

namespace Caitlyn.AuthNAuthZ.Presentation.Api.Controllers.v1;

[ApiController]
[ApiVersion("1")]
[Route("api/v{version:apiVersion}/[controller]")]
public class UserController: CustomControllerBase
{
    private readonly IServiceProvider _serviceProvider;

    public UserController(IServiceProvider serviceProvider, IErrorMapper errorMapper): base(errorMapper)
    {
        _serviceProvider = serviceProvider;
    }


    [HttpPost]
    public  Task<IActionResult> Post(NewUser.NewUserDto newUserDto)
    {
        var addUserCommand = _serviceProvider.GetRequiredService<IAddUserCommand>();
        var result = addUserCommand.Execute(newUserDto);
        return Task.FromResult(JsonFromResult(HttpStatusCode.Created, result));
    }

    [HttpGet("{uuid}")]
    public Task<IActionResult> Get(string uuid)
    {
        var getUserQuery = _serviceProvider.GetRequiredService<IGetUserQuery>();
        var result = getUserQuery.Execute(uuid);
        return Task.FromResult(JsonFromResult(HttpStatusCode.OK, result));
    }

    [HttpDelete("{uuid}")]
    public Task<IActionResult> Delete(string uuid)
    {
        var removeUserCommand = _serviceProvider.GetRequiredService<IRemoveUserCommand>();
        var errorOption = removeUserCommand.Execute(uuid);
        return Task.FromResult(JsonFromErrorOption(HttpStatusCode.OK, errorOption));
    }

    [HttpPut("{uuid}")]
    public Task<IActionResult> UpdateProfile(string uuid, EditUserProfile.EditUserProfileDto editUserProfileDto)
    {
        var updateUserCommand = _serviceProvider.GetRequiredService<IEditUserProfileCommand>();
        var result = updateUserCommand.Execute(uuid, editUserProfileDto);
        return Task.FromResult(JsonFromResult(HttpStatusCode.OK, result));
    }
}