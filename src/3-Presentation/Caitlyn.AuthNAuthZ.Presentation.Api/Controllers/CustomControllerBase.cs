using System.Net;
using Caitlyn.AuthNAuthZ.Domain.Models;
using Caitlyn.AuthNAuthZ.Domain.Models.Errors;
using Caitlyn.AuthNAuthZ.Domain.Models.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FSharp.Core;

namespace Caitlyn.AuthNAuthZ.Presentation.Api.Controllers;

public class CustomControllerBase: ControllerBase
{
    private readonly IErrorMapper _errorMapper;

    protected CustomControllerBase(IErrorMapper errorMapper)
    {
        _errorMapper = errorMapper;
    }

    protected IActionResult JsonFromErrorOption(HttpStatusCode successCode, FSharpOption<Errors.DomainErrors> errorsOption)
    {
        if (FSharpOption<Errors.DomainErrors>.get_IsNone(errorsOption))
            return new JsonResult(new ApiResult())
            {
                StatusCode = (int)successCode
            };

        var errorValue = errorsOption.Value;
        var errorMessage = _errorMapper.Map(errorValue);
        var errorType = _errorMapper.GetErrorType(errorValue);

        return new JsonResult(new ApiResult()
        {
            Errors = new[] { errorMessage }
        })
        {
            StatusCode = GetStatusCode(errorType)
        };
    }

    protected IActionResult JsonFromResult<TResult>(HttpStatusCode successCode,  FSharpResult<TResult, Errors.DomainErrors> result)
    {
        if (result.IsOk)
            return new JsonResult(new ApiResult<TResult>
            {
                Result = result.ResultValue
            })
            {
                StatusCode = (int) successCode
            };

        var error = result.ErrorValue;
        var errorType = _errorMapper.GetErrorType(error);
        
        var statusCode = GetStatusCode(errorType);
        var errorMessage = _errorMapper.Map(error);

        return new JsonResult(new ApiResult
        {
            Errors = new[] {errorMessage}
        })
        {
            StatusCode = statusCode
        };
    }
    

    private int GetStatusCode(Errors.ErrorType errorType)
    {
        if (errorType.IsInternalError) return (int)HttpStatusCode.InternalServerError;
        if (errorType.IsInvalidRequest) return (int)HttpStatusCode.BadRequest;
        if (errorType.IsNotFound) return (int)HttpStatusCode.NotFound;

        return (int)HttpStatusCode.BadRequest;
    }
}