namespace Caitlyn.AuthNAuthZ.Domain.Models.Resources

open System
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Uuid
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result

module ResourceType =
    
    type ResourceTypeDto = {
        Name: string
        Uuid: Guid
    }
    type ResourceType = private { Name: Name; Uuid: Uuid }

    let create (x: ResourceTypeDto) =
        result {
            let! name = Name.create x.Name
            let! uuid = Uuid.createFromGuid x.Uuid

            return { Name = name; Uuid = uuid }
        }

    let toDto (x: ResourceType) : ResourceTypeDto =
        { Name = Name.evaluate x.Name
          Uuid = Uuid.evaluate x.Uuid }
