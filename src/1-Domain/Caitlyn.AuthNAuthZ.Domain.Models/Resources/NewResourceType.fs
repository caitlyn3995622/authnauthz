namespace Caitlyn.AuthNAuthZ.Domain.Models.Resources

open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result

module NewResourceType =
    type NewResourceTypeDto = { Name: string }
    type NewResourceType = private { Name: Name }

    let create (x: NewResourceTypeDto) =
        result {
            let! name = Name.create x.Name

            return { Name = name }
        }

    let toDto (x: NewResourceType) : NewResourceTypeDto = { Name = Name.evaluate x.Name }
