namespace Caitlyn.AuthNAuthZ.Domain.Models.Resources

open System
open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Uuid
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result


module Resource =
    type ResourceDto = { Name: string; Uuid: Guid }
    type Resource = private { Uuid: Uuid; Name: Name }

    let create (x: ResourceDto) =
        result {
            let! name = Name.create x.Name
            let! uuid = Uuid.createFromGuid x.Uuid

            return { Name = name; Uuid = uuid }
        }

    let toDto (x: Resource) : ResourceDto =
        { Name = Name.evaluate x.Name
          Uuid = Uuid.evaluate x.Uuid }
