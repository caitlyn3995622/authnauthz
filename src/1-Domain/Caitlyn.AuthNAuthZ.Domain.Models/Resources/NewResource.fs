namespace Caitlyn.AuthNAuthZ.Domain.Models.Resources

open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result

module NewResource =
    type NewResourceDto = { Name: string }
    type NewResource = { Name: Name }

    let create (x: NewResourceDto) =
        result {
            let! name = Name.create x.Name

            return { Name = name }
        }

    let toDto (x: NewResource) : NewResourceDto = { Name = Name.evaluate x.Name }
