namespace Caitlyn.AuthNAuthZ.Domain.Models

open Caitlyn.AuthNAuthZ.Domain.Models.Errors
open Microsoft.Extensions.DependencyInjection

module ServiceCollectionExtensions =
    let AddErrorMapper (sc: IServiceCollection) =
        sc.AddSingleton<IErrorMapper, ErrorMapper>()

