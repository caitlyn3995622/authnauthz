namespace Caitlyn.AuthNAuthZ.Domain.Models.Groups

open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result

module NewGroup =

    type NewGroupDto = { Name: string }
    type NewGroup = private { Name: Name }

    let create (x: NewGroupDto) =
        result {
            let! name = Name.create x.Name

            return { Name = name }
        }

    let toDto (x: NewGroup) : NewGroupDto = { Name = Name.evaluate x.Name }
