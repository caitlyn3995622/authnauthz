namespace Caitlyn.AuthNAuthZ.Domain.Models.Groups

open System
open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Uuid
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result

module Group =

    type GroupDto = { Name: string; Uuid: Guid }
    type Group = private { Name: Name; Uuid: Uuid }

    let create (x: GroupDto) =
        result {
            let! name = Name.create x.Name
            let! uuid = Uuid.createFromGuid x.Uuid

            return { Name = name; Uuid = uuid }
        }

    let toDto (x: Group) : GroupDto =
        { Name = Name.evaluate x.Name
          Uuid = Uuid.evaluate x.Uuid }
