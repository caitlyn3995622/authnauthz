namespace Caitlyn.AuthNAuthZ.Domain.Models.Permissions


module Permissions =
    type resourceTypePermission =
        | AddPermission
        | ListPermission
        
   type resourcePermission =
       | EditPermission
       | ReadPermission
       | DeletePermission