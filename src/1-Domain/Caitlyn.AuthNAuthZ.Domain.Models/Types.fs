namespace Caitlyn.AuthNAuthZ.Domain.Models

open System
open System.Text.RegularExpressions
open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors

module Types =
    module Uuid =
        type Uuid = private Uuid of Guid

        let createFromGuid x =
            if x = Guid.Empty then
                Error <| TypeError (EmptyUuid, InvalidRequest) 
            else
                Ok <| Uuid x
                
        let createFromString (x:string) =
            let mutable guid = Guid.Empty
            if Guid.TryParse(x, &guid) then
                createFromGuid guid
            else
                Error <| TypeError (InvalidUuid, ErrorType.InvalidRequest)

        let evaluate (Uuid x) = x

    module Name =
        type Name = private Name of string

        let create x =
            match x with
            | s when String.IsNullOrWhiteSpace s -> Error <| TypeError (EmptyName, InvalidRequest)
            | s when s.Length < 2 -> Error <| TypeError (NameTooShort, InvalidRequest)
            | _ -> Ok <| Name x

        let evaluate (Name x) = x

    module Password =
        type Password = private Password of string

        let private passwordRegex =
            Regex @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*])[A-Za-z\d!@#$%^&*]{8,}$"

        let create x =
            match x with
                | s when String.IsNullOrWhiteSpace s -> Error <| TypeError (EmptyPassword, InvalidRequest)
                | s when passwordRegex.IsMatch s -> Ok <| Password x
                | _ -> Error <| TypeError (InvalidPassword, InvalidRequest)
             
        let evaluate (Password x) = x
    module Email =
        let private emailRegex = Regex "^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$"
        
        type Email = private Email of string
        
        let create x =
            match x with
                | s when String.IsNullOrWhiteSpace s -> Error <| TypeError (EmptyEmail, InvalidRequest)
                | s when emailRegex.IsMatch s -> Ok <| Email x
                | _ -> Error <| TypeError (InvalidEmail, InvalidRequest)
             
        let evaluate (Email x) = x
            
