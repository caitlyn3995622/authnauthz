namespace Caitlyn.AuthNAuthZ.Domain.Models.Apis

open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result

module NewApi =
    type NewApiDto = { Name: string }

    type NewApi = { Name: Name }

    let create (x: NewApiDto) =
        result {
            let! name = Name.create x.Name
            return { Name = name }
        }

    let toDto (x: NewApi) : NewApiDto = { Name = Name.evaluate x.Name }
