namespace Caitlyn.AuthNAuthZ.Domain.Models.Apis

open System
open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Uuid
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result

module Api =
    type ApiDto = { Name: string; Uuid: Guid }
    type Api = private { Name: Name; Uuid: Uuid }

    let create (x: ApiDto) =
        result {
            let! name = Name.create x.Name
            let! uuid = Uuid.createFromGuid x.Uuid

            return { Name = name; Uuid = uuid }
        }

    let toDto (x: Api) : ApiDto =
        { Name = Name.evaluate x.Name
          Uuid = Uuid.evaluate x.Uuid }
