namespace Caitlyn.AuthNAuthZ.Domain.Models.Apis

open System
open Caitlyn.AuthNAuthZ.Domain.Models.Apis.ApiKeyValue
open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Uuid
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result

module ApiKey =
    type ApiKeyDto =
        { Name: string
          Value: Guid
          ApiUuid: Guid
          Uuid: Guid }

    type ApiKey = private {
          Name: Name
          Value: ApiKeyValue
          Uuid: Uuid
          ApiUuid: Uuid }

    let create (x: ApiKeyDto) =
        result {
            let! name = Name.create x.Name
            let! value = ApiKeyValue.create x.Value
            let! uuid = Uuid.createFromGuid x.Uuid
            let! apiUuid = Uuid.createFromGuid x.ApiUuid

            return
                { Name = name
                  Value = value
                  Uuid = uuid
                  ApiUuid = apiUuid }
        }

    let toDto (x: ApiKey) : ApiKeyDto =
        { Name = Name.evaluate x.Name
          Value = ApiKeyValue.evaluate x.Value
          Uuid = Uuid.evaluate x.Uuid
          ApiUuid = Uuid.evaluate x.ApiUuid }
