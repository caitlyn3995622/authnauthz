namespace Caitlyn.AuthNAuthZ.Domain.Models.Apis

open System
open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors

module ApiKeyValue =
    type ApiKeyValue = private ApiKeyValue of Guid

    let create x =
        if x = Guid.Empty then
            Error <| ApiKeyError (EmptyApiKey, InvalidRequest)
        else
            Ok <| ApiKeyValue x

    let evaluate (ApiKeyValue x) = x
