namespace Caitlyn.AuthNAuthZ.Domain.Models.Apis

open System
open Caitlyn.AuthNAuthZ.Domain.Models.Apis.ApiKeyValue
open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Uuid
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result

module NewApiKey =
    type NewApiKeyDto = {
          Name: string
          Value: Guid
          ApiUuid: Guid }

    type NewApiKey = private {
              Name: Name
              Value: ApiKeyValue
              ApiUuid: Uuid }

    let create (x: NewApiKeyDto) =
        result {
            let! name = Name.create x.Name
            let! value = ApiKeyValue.create x.Value
            let! apiUuid = Uuid.createFromGuid x.ApiUuid

            return
                { Name = name
                  Value = value
                  ApiUuid = apiUuid }
        }

    let toDto (x: NewApiKey) : NewApiKeyDto =
        { Name = Name.evaluate x.Name
          Value = ApiKeyValue.evaluate x.Value
          ApiUuid = Uuid.evaluate x.ApiUuid }
