namespace Caitlyn.AuthNAuthZ.Domain.Models.Users

open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Email
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result

module EditUserProfile =
    type EditUserProfileDto = {
        Email: string
    }
    
    type EditUserProfile = private {
        Email: Email
    }
    
    let evaluateEmail x =
        x.Email
        |> Email.evaluate
        
    let create (x:EditUserProfileDto) =
        result {
            let! email = Email.create x.Email
            
            return {Email = email}
        }

