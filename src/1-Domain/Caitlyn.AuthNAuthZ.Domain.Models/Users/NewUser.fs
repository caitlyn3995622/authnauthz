namespace Caitlyn.AuthNAuthZ.Domain.Models.Users

open Caitlyn.AuthNAuthZ.Domain.Models.Types.Email
open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Password
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result

module NewUser =
    
    type NewUserDto = {
        UserName: string
        Email: string
        Password: string
    }
    
    type NewUser = private {
        UserName: Name
        Password: Password
        Email: Email
    }
    
    let create (x:NewUserDto) =
        result {
            let! userName = Name.create x.UserName
            let! password = Password.create x.Password
            let! email = Email.create x.Email
            
            return {
                UserName = userName
                Password = password
                Email = email
            }
        }
        
    let toDto (x:NewUser): NewUserDto = {
        UserName = Name.evaluate x.UserName
        Email = Email.evaluate x.Email
        Password = Password.evaluate x.Password
    }
    
    let getUserName (x:NewUser) =
        Name.evaluate x.UserName
     
    let getEmail (x:NewUser) =
        Email.evaluate x.Email
    
    let getPassword (x:NewUser) =
        Password.evaluate x.Password