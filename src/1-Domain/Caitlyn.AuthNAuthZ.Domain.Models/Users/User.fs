namespace Caitlyn.AuthNAuthZ.Domain.Models.Users

open System
open Caitlyn.AuthNAuthZ.Domain.Models
open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Email
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Name
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Uuid
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result;

module User =
    type UserDto =
        {
        Uuid: Guid
        UserName: string
        Email: string
        }
    
    type User = private {
        UserName: Name
        Email: Email
        Uuid: Uuid
    }
    
    let create (x:UserDto) =
        result {
            let! name = Types.Name.create x.UserName
            let! email = Types.Email.create x.Email
            let! uuid = Types.Uuid.createFromGuid x.Uuid
            
            return {
                UserName = name
                Email = email
                Uuid = uuid
            }
        }
        
    let toDto (x: User): UserDto =
        {
            UserName = Types.Name.evaluate x.UserName
            Email = Types.Email.evaluate x.Email
            Uuid = Types.Uuid.evaluate x.Uuid
        }
        
    let getUserName (x:User) =
        Name.evaluate x.UserName
        
    let getUuid (x:User) =
        Uuid.evaluate x.Uuid
        
    let getEmail (x:User) =
        Email.evaluate x.Email