namespace Caitlyn.AuthNAuthZ.Domain.Models.Errors

open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors

type ErrorMapper() =
    member this.MapTypeError typeError =
        match typeError with
            | EmptyUuid -> "EmptyUuid"
            | EmptyName -> "EmptyName"
            | NameTooShort -> "NameTooShort"
            | InvalidPassword -> "InvalidPassword"
            | EmptyPassword -> "EmptyPassword"
            | EmptyEmail -> "EmptyEmail"
            | InvalidEmail -> "InvalidEmail"
            | InvalidUuid -> "InvalidUuid"
            
    member this.MapUserError userError =
        match userError with
            | UserNameAlreadyInUse -> "UserNameAlreadyInUse"
            | UserNotFound -> "UserNotFound"
            | EmailAlreadyExists  -> "EmailAlreadyExists"
            
    member this.MapApiKeyError apiKeyError =
        match apiKeyError with
            | EmptyApiKey -> "EmptyApiKey"    
            
    interface IErrorMapper with
        member this.Map(domainError) =
            match domainError with
                | TypeError (typeError, _) -> this.MapTypeError typeError
                | UserError(userError, _) -> this.MapUserError userError
                | ApiKeyError(apiKeyError, _) -> this.MapApiKeyError apiKeyError

        member this.GetErrorType(domainError) =
            match domainError with
                | TypeError (_, errorType) -> errorType
                | UserError(_, errorType) -> errorType
                | ApiKeyError(_, errorType) -> errorType
                
        
        

