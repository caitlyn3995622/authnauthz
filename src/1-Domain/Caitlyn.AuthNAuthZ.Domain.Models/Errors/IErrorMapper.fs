namespace Caitlyn.AuthNAuthZ.Domain.Models.Errors

open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors

type IErrorMapper =
    abstract member Map: DomainErrors -> string
    abstract member GetErrorType: DomainErrors -> ErrorType
