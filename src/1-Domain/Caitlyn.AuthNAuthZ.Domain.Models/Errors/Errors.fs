namespace Caitlyn.AuthNAuthZ.Domain.Models.Errors

module Errors =
    type ErrorType =
        | InvalidRequest
        | NotFound
        | InternalError
    
    type TypeErrors =
        | EmptyUuid
        | InvalidUuid
        | EmptyName
        | NameTooShort
        | InvalidPassword
        | EmptyPassword
        | EmptyEmail
        | InvalidEmail
        
    type ApiKeyErrors =
       | EmptyApiKey
        
    type UserErrors =
       | UserNameAlreadyInUse
       | UserNotFound
       | EmailAlreadyExists
       
       type DomainErrors =
           | TypeError of TypeErrors * ErrorType
           | ApiKeyError of ApiKeyErrors * ErrorType
           | UserError of UserErrors * ErrorType

