namespace Caitlyn.AuthNAuthZ.Domain.Cqrs

open Caitlyn.AuthNAuthZ.Domain.Cqrs.Users
open Microsoft.Extensions.DependencyInjection

module ServiceCollectionExtensions =
    let AddCqrs (services: IServiceCollection) =
        services
            .AddScoped<IAddUserCommand, AddUserCommand>()
            .AddScoped<IGetUserQuery, GetUserQuery>()
            .AddScoped<IRemoveUserCommand, RemoveUserCommand>()
            .AddScoped<IEditUserProfileCommand, EditUserProfileCommand>()
        