namespace Caitlyn.AuthNAuthZ.Domain.Cqrs.Users

open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors

type IRemoveUserCommand =
    abstract member Execute: string -> DomainErrors option

