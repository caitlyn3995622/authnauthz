namespace Caitlyn.AuthNAuthZ.Domain.Cqrs.Users

open System
open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors
open Caitlyn.AuthNAuthZ.Domain.Models.Types

type RemoveUserCommand(userRepository: IUserRepository) =
    member private this._userRepository = userRepository

    member private this.RemoveUser uuid =
        let uuidResult = Uuid.createFromGuid uuid
        
        match uuidResult with
        | Error domainError -> Some domainError
        | Ok domainUuid -> this._userRepository.RemoveAsync domainUuid
                                            |>Async.AwaitTask
                                            |>Async.RunSynchronously
    
    interface IRemoveUserCommand with
        member this.Execute(uuid) =
            let mutable guid = Guid.Empty
            let parsedGuid = Guid.TryParse(uuid, &guid)

            match parsedGuid with
            | false -> Some <| TypeError(TypeErrors.InvalidUuid, ErrorType.InvalidRequest)
            | true -> this.RemoveUser guid
