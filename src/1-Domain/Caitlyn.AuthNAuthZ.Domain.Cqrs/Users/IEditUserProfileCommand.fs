namespace Caitlyn.AuthNAuthZ.Domain.Cqrs.Users

open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors
open Caitlyn.AuthNAuthZ.Domain.Models.Users.EditUserProfile
open Caitlyn.AuthNAuthZ.Domain.Models.Users.User

type IEditUserProfileCommand =
    abstract member Execute: string -> EditUserProfileDto -> Result<UserDto, DomainErrors>

