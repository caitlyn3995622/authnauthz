namespace Caitlyn.AuthNAuthZ.Domain.Cqrs.Users

open System
open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors
open Caitlyn.AuthNAuthZ.Domain.Models.Users.User

type IGetUserQuery =
    abstract member Execute: String -> Result<UserDto, DomainErrors>