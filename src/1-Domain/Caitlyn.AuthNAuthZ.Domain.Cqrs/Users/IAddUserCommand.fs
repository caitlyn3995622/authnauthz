namespace Caitlyn.AuthNAuthZ.Domain.Cqrs.Users

open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors
open Caitlyn.AuthNAuthZ.Domain.Models.Users.NewUser
open Caitlyn.AuthNAuthZ.Domain.Models.Users.User

type IAddUserCommand =
    abstract member Execute: NewUserDto -> Result<UserDto, DomainErrors>

