namespace Caitlyn.AuthNAuthZ.Domain.Cqrs.Users

open System.Threading.Tasks
open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors
open Caitlyn.AuthNAuthZ.Domain.Models.Types.Uuid
open Caitlyn.AuthNAuthZ.Domain.Models.Users.EditUserProfile
open Caitlyn.AuthNAuthZ.Domain.Models.Users.NewUser
open Caitlyn.AuthNAuthZ.Domain.Models.Users.User

type IUserRepository =
    abstract member CheckIfUserNameAlreadyInUseAsync: string -> Task<bool>
    abstract member CheckIfEmailAlreadyInUseAsync: string -> Task<bool>
    abstract member AddAsync: NewUser -> Task<UserDto>
    abstract member GetAsync: Uuid -> Task<UserDto option>
    abstract member RemoveAsync: Uuid -> Task<DomainErrors option>
    abstract member EditProfileAsync: Uuid -> EditUserProfile -> Task<Result<UserDto, DomainErrors>>