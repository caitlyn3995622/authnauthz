namespace Caitlyn.AuthNAuthZ.Domain.Cqrs.Users

open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result
open Caitlyn.AuthNAuthZ.Domain.Cqrs.Users
open Caitlyn.AuthNAuthZ.Domain.Models.Users
open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors

type internal AddUserCommand(userRepository: IUserRepository) =
    member private this._userRepository = userRepository

    member private this.AddUser user =
            this._userRepository.AddAsync user
                        |> Async.AwaitTask
                        |> Async.RunSynchronously
        
    member private this.EnsureUserNameIsUnique userName =
        let nameExists = this._userRepository.CheckIfUserNameAlreadyInUseAsync(userName)
                        |> Async.AwaitTask
                        |> Async.RunSynchronously
        
        match nameExists with
            | false -> Ok false
            | true -> Error <| UserError (UserNameAlreadyInUse, ErrorType.InvalidRequest)
            
    member private this.EnsureEmailIsUnique email =
        let emailExists = this._userRepository.CheckIfEmailAlreadyInUseAsync(email)
                        |> Async.AwaitTask
                        |> Async.RunSynchronously
                        
        match emailExists with
            |false -> Ok false
            | true -> Error <| UserError (EmailAlreadyExists, ErrorType.InvalidRequest)


    interface IAddUserCommand with
        member this.Execute userDto =
            result {
                let! user = NewUser.create userDto
                let! _ = this.EnsureUserNameIsUnique userDto.UserName
                let! _ = this.EnsureEmailIsUnique userDto.Email
                
                return this.AddUser user
            }
