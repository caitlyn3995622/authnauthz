namespace Caitlyn.AuthNAuthZ.Domain.Cqrs.Users

open System
open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result
open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors
open Caitlyn.AuthNAuthZ.Domain.Models.Types

type internal GetUserQuery(userRepository: IUserRepository) =
    member private this._userRepository = userRepository
    
    member private this.GetUser uuid =
        result {
            let! parsedUuid = Uuid.createFromGuid uuid
            let user = this._userRepository.GetAsync parsedUuid
                    |> Async.AwaitTask
                    |> Async.RunSynchronously
            
            match user with
                | None -> return! Error <| UserError (UserNotFound, ErrorType.NotFound)
                | Some userDto -> return! Ok userDto
        }
    
    interface IGetUserQuery with
        member this.Execute(uuid) =
            let mutable guid = Guid.Empty;
            let isGuid = Guid.TryParse(uuid, &guid)
            
            match isGuid with
                | false -> Error <| TypeError (InvalidUuid, ErrorType.InvalidRequest)
                | true -> this.GetUser guid