namespace Caitlyn.AuthNAuthZ.Domain.Cqrs.Users

open Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions.Result
open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors
open Caitlyn.AuthNAuthZ.Domain.Models.Types
open Caitlyn.AuthNAuthZ.Domain.Models.Users

type internal EditUserProfileCommand(userRepository: IUserRepository) =
    member private this._userRepository = userRepository
    
    interface IEditUserProfileCommand with
        member this.Execute uuid userProfileDto =
            result {
                let! editUserProfile = EditUserProfile.create userProfileDto
                let! validatedUuid = Uuid.createFromString uuid
                let existingUser = this._userRepository.GetAsync validatedUuid
                                |> Async.AwaitTask
                                |> Async.RunSynchronously
                    
               match existingUser with
               | None -> return! Error <| UserError (UserErrors.UserNotFound, ErrorType.NotFound)
               | Some _ -> return! this._userRepository.EditProfileAsync validatedUuid editUserProfile
                                            |> Async.AwaitTask
                                            |> Async.RunSynchronously
            }
