namespace Caitlyn.AuthNAuthZ.CrossCutting.ComputationExpressions

module Result =
    type ResultBuilder() =
        member _.Return (x) = Ok x
        member _.Bind(x, f) = Result.bind f x
        member _.ReturnFrom x = x
    
    let result = ResultBuilder()
