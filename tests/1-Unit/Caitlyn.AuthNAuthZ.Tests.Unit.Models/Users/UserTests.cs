using Caitlyn.AuthNAuthZ.Domain.Models.Users;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Users;

public class UsersTests
{
    [Fact]
    public void CreateUserShouldCreateUserGivenValidValues()
    {
        var expectedDto = new User.UserDto(Guid.NewGuid(), "Test", "test@test.com");
        var createdUserResult = User.create(expectedDto);
        
        Assert.True(createdUserResult.IsOk);
        Assert.Equal(expectedDto, User.toDto(createdUserResult.ResultValue));
    }
    
    [Theory]
    [MemberData(nameof(GetInvalidData))]
    public void CreateUserShouldReturnErrorGivenInvalidValue(Guid uuid, string name, string email)
    {
        var expectedDto = new User.UserDto(uuid, name, email);
        var createdUserResult = User.create(expectedDto);
        
        Assert.True(createdUserResult.IsError);
        Assert.NotNull(createdUserResult.ErrorValue);
    }

    public static IEnumerable<object[]> GetInvalidData()
    {
        yield return new object[] { Guid.Empty, "Test", "test@test.com" };
        yield return new object[] { Guid.NewGuid(), string.Empty, "test@test.com" };
        yield return new object[] { Guid.NewGuid(), "Test", Guid.NewGuid().ToString() };
    }
}