using Caitlyn.AuthNAuthZ.Domain.Models.Users;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Users;

public class NewUserTests
{
        
    [Fact]
    public void CreateNewUserShouldCreateNewUserGivenValidValues()
    {
        var expectedDto = new NewUser.NewUserDto("Test", "test@test.com", "Test*123456789");
        var createdUserResult = NewUser.create(expectedDto);
            
        Assert.True(createdUserResult.IsOk);
        Assert.Equal(expectedDto, NewUser.toDto(createdUserResult.ResultValue));
    }
        
    [Theory]
    [MemberData(nameof(GetInvalidData))]
    public void CreateNewUserShouldReturnErrorGivenInvalidValue(string name, string email, string password)
    {
        var expectedDto = new NewUser.NewUserDto(name, email, password);
        var createdUserResult = NewUser.create(expectedDto);
            
        Assert.True(createdUserResult.IsError);
        Assert.NotNull(createdUserResult.ErrorValue);
    }
    
    public static IEnumerable<object[]> GetInvalidData()
    {
        yield return new object[] { string.Empty, string. Empty, string.Empty };
        yield return new object[] { string.Empty, "test@test.com", string.Empty };
        yield return new object[] { "Test", "test@test.com", "abc" };
    }
}