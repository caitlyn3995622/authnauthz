using Caitlyn.AuthNAuthZ.Domain.Models.Resources;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Resources;

public class NewResourceTests
{
        
    [Fact]
    public void CreateNewResourceShouldReturnOkGivenValidValues()
    {
        var expectedDto = new NewResource.NewResourceDto("Test");
        var resourceResult = Domain.Models.Resources.NewResource.create(expectedDto);

        Assert.True(resourceResult.IsOk);
        Assert.Equal(expectedDto, Domain.Models.Resources.NewResource.toDto(resourceResult.ResultValue));
    }
    
        
    [Theory]
    [MemberData(nameof(GetInvalidData))]
    public void CreateNewResourceShouldReturnErrorGivenInvalidValues(string name)
    {
        var dto = new NewResource.NewResourceDto(name);
        var resourceResult = Domain.Models.Resources.NewResource.create(dto);
        
        Assert.True(resourceResult.IsError);
        Assert.NotNull(resourceResult.ErrorValue);
    }
    
    public static IEnumerable<object[]> GetInvalidData()
    {
        yield return new object[] { string.Empty };
        yield return new object[] { null };
        yield return new object[] { "           " };
    }
}