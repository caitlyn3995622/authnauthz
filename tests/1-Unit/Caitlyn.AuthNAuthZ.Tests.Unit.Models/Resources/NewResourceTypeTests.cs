using Caitlyn.AuthNAuthZ.Domain.Models.Resources;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Resources;

public class NewResourceTypeTests
{
    [Fact]
    public void CreateNewResourceTypeShouldReturnOkGivenValidValues()
    {
        var expectedDto = new NewResourceType.NewResourceTypeDto("Test");
        var resourceTypeResult = NewResourceType.create(expectedDto);
        
        Assert.True(resourceTypeResult.IsOk);
        Assert.Equal(expectedDto, NewResourceType.toDto(resourceTypeResult.ResultValue));
    }

    [Theory]
    [MemberData(nameof(GetInvalidData))]
    public void CreateNewResourceTypeShouldReturnErrorGivenInvalidValues(string name)
    {
        var dto = new NewResourceType.NewResourceTypeDto(name);
        var resourceTypeResult = NewResourceType.create(dto);
        
        Assert.True(resourceTypeResult.IsError);
        Assert.NotNull(resourceTypeResult.ErrorValue);
    }
    
    public static IEnumerable<object[]> GetInvalidData()
    {
        yield return new object[] { string.Empty };
        yield return new object[] { null };
        yield return new object[] { "           " };
    }
}