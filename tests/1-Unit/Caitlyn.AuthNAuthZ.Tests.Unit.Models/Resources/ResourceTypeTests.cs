using Caitlyn.AuthNAuthZ.Domain.Models.Resources;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Resources;

public class ResourceTypesTests
{
    [Fact]
    public void CreateResourceTypeShouldReturnOkGivenValidValues()
    {
        var expectedDto = new ResourceType.ResourceTypeDto("Test", Guid.NewGuid());
        var resourceTypeResult = ResourceType.create(expectedDto);
        
        Assert.True(resourceTypeResult.IsOk);
        Assert.Equal(expectedDto, ResourceType.toDto(resourceTypeResult.ResultValue));
    }
    
    [Theory]
    [MemberData(nameof(GetInvalidData))]
    public void CreateResourceTypeShouldReturnErrorGivenInvalidValues(string name, Guid uuid)
    {
        var dto = new ResourceType.ResourceTypeDto(name, uuid);
        var resourceTypeResult = ResourceType.create(dto);
        
        Assert.True(resourceTypeResult.IsError);
        Assert.NotNull(resourceTypeResult.ErrorValue);
    }
    
    public static IEnumerable<object[]> GetInvalidData()
    {
        yield return new object[] { string.Empty, Guid.Empty };
        yield return new object[] { Guid.NewGuid().ToString(), Guid.Empty };
        yield return new object[] { string.Empty, Guid.NewGuid()};
    }
}