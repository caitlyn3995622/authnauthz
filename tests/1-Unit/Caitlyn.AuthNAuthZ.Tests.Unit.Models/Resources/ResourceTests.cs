using Caitlyn.AuthNAuthZ.Domain.Models.Resources;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Resources;

public class ResourcesTests
{
    [Fact]
    public void CreateResourceShouldReturnOkGivenValidValues()
    {
        var expectedDto = new Resource.ResourceDto("Test", Guid.NewGuid());
        var resourceResult = Domain.Models.Resources.Resource.create(expectedDto);

        Assert.True(resourceResult.IsOk);
        Assert.Equal(expectedDto, Domain.Models.Resources.Resource.toDto(resourceResult.ResultValue));
    }

    [Theory]
    [MemberData(nameof(GetInvalidData))]
    public void CreateResourceShouldReturnErrorGivenInvalidValues(string name, Guid uuid)
    {
        var dto = new Resource.ResourceDto(name, uuid);
        var resourceResult = Domain.Models.Resources.Resource.create(dto);
        
        Assert.True(resourceResult.IsError);
        Assert.NotNull(resourceResult.ErrorValue);
    }
    
    public static IEnumerable<object[]> GetInvalidData()
    {
        yield return new object[] { string.Empty, Guid.Empty };
        yield return new object[] { Guid.NewGuid().ToString(), Guid.Empty };
        yield return new object[] { string.Empty, Guid.NewGuid()};
    }

}