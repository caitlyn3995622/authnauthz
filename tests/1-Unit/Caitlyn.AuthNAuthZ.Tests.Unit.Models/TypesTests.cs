using Caitlyn.AuthNAuthZ.Domain.Models;
using Caitlyn.AuthNAuthZ.Domain.Models.Errors;
using Caitlyn.AuthNAuthZ.Domain.Models.Users;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models;

public class TypesTests
{
    [Theory]
    [MemberData(nameof(InvalidUuidData))]
    public void CreateUuidShouldReturnErrorGivenInvalidGuid(Guid invalidUuid)
    {
        var uuidResult = Types.Uuid.createFromGuid(invalidUuid);
        
        Assert.True(uuidResult.IsError);
        Assert.True(uuidResult.ErrorValue.IsTypeError);
        var typeError = (Errors.DomainErrors.TypeError)uuidResult.ErrorValue;
        Assert.Equal(Errors.TypeErrors.EmptyUuid, typeError.Item1);
        Assert.Equal(Errors.ErrorType.InvalidRequest, typeError.Item2);
    }
    
    public static IEnumerable<object[]> InvalidUuidData()
    {
        yield return new object[] { Guid.Empty };
    }

    [Fact]
    public void CreateUuidShouldReturnOkGivenValidGuid()
    {
        var guid = Guid.NewGuid();
        var uuidResult = Types.Uuid.createFromGuid(guid);
        
        Assert.True(uuidResult.IsOk);
        Assert.Equal(guid, Types.Uuid.evaluate(uuidResult.ResultValue));
    }

    [Theory]
    [MemberData(nameof(GetInvalidNames))]
    public void CreateNameShouldReturnErrorGivenInvalidName(string name, Errors.TypeErrors expectedError)
    {
        var nameResult = Types.Name.create(name);

        Assert.True(nameResult.IsError);
        Assert.True(nameResult.ErrorValue.IsTypeError);
        var typeError = (Errors.DomainErrors.TypeError)nameResult.ErrorValue;
        Assert.Equal(expectedError, typeError.Item1);
        Assert.Equal(Errors.ErrorType.InvalidRequest, typeError.Item2);

    }
    
    public static IEnumerable<object[]> GetInvalidNames()
    {
        yield return new object[] { null, Errors.TypeErrors.EmptyName };
        yield return new object[] { string.Empty , Errors.TypeErrors.EmptyName };
        yield return new object[] { "       " , Errors.TypeErrors.EmptyName };
        yield return new object[] { "a" , Errors.TypeErrors.NameTooShort };
    }

    [Fact]
    public void CreateNameShouldReturnOkGivenValidName()
    {
        var name = "Test";
        var nameResult = Types.Name.create(name);
        
        Assert.True(nameResult.IsOk);
        Assert.Equal(name, Types.Name.evaluate(nameResult.ResultValue));
    }
    
    [Theory]
    [InlineData("")]
    [InlineData(null)]
    [InlineData("     ")]
    public void CreatePasswordShouldReturnEmptyPasswordErrorGivenInvalidPassword(string password)
    {
        var passwordResult = Types.Password.create(password);

        Assert.True(passwordResult.IsError);
        Assert.True(passwordResult.ErrorValue.IsTypeError);
        var typeError = (Errors.DomainErrors.TypeError)passwordResult.ErrorValue;
        Assert.Equal(Errors.TypeErrors.EmptyPassword, typeError.Item1);
        Assert.Equal(Errors.ErrorType.InvalidRequest, typeError.Item2);
    }
    
    [Theory]
    [InlineData("abc")]
    [InlineData("abC")]
    [InlineData("abc1")]
    [InlineData("abC5")]
    [InlineData("abc5*")]
    [InlineData("aBc*")]
    [InlineData("aBc5*")]
    [InlineData("abc*")]
    [InlineData("a*#12T")]
    public void CreatePasswordShouldReturnInvalidPasswordErrorGivenInvalidPassword(string password)
    {
        var passwordResult = Types.Password.create(password);

        Assert.True(passwordResult.IsError);
        Assert.True(passwordResult.ErrorValue.IsTypeError);
        var typeError = (Errors.DomainErrors.TypeError)passwordResult.ErrorValue;
        Assert.Equal(Errors.TypeErrors.InvalidPassword, typeError.Item1);
        Assert.Equal(Errors.ErrorType.InvalidRequest, typeError.Item2);

    }

    [Fact]
    public void CreatePasswordShouldReturnOkGivenValidName()
    {
        var password = "Test*123456789";
        var passwordResult = Types.Password.create(password);
        
        Assert.True(passwordResult.IsOk);
        Assert.Equal(password, Types.Password.evaluate(passwordResult.ResultValue));
        
    }
    
    [Theory]
    [InlineData("")]
    [InlineData(null)]
    [InlineData("     ")]
    public void CreateEmailShouldReturnEmptyEmailErrorGivenInvalidEmail(string email)
    {
        var emailResult = Types.Email.create(email);

        Assert.True(emailResult.IsError);
        Assert.True(emailResult.ErrorValue.IsTypeError);
        var typeError = (Errors.DomainErrors.TypeError)emailResult.ErrorValue;
        Assert.Equal(Errors.TypeErrors.EmptyEmail, typeError.Item1);
        Assert.Equal(Errors.ErrorType.InvalidRequest, typeError.Item2);

    }
    
    [Theory]
    [InlineData("abc")]
    [InlineData("abC@")]
    [InlineData("abC@test")]
    [InlineData("abC@test..")]
    [InlineData("abC@test..com")]
    [InlineData("@test")]
    [InlineData("a.com.br")]

    public void CreateEmailShouldReturnInvalidEmailErrorGivenInvalidEmail(string email)
    {
        var emailResult = Types.Email.create(email);

        Assert.True(emailResult.IsError);
        Assert.True(emailResult.ErrorValue.IsTypeError);
        var typeError = (Errors.DomainErrors.TypeError)emailResult.ErrorValue;
        Assert.Equal(Errors.TypeErrors.InvalidEmail, typeError.Item1);
        Assert.Equal(Errors.ErrorType.InvalidRequest, typeError.Item2);
    }

    [Theory]
    [InlineData("abctdhgyt@protonmail.com")]
    [InlineData("abacate@gmail.com")]
    [InlineData("test@yahoo.com.br")]
    [InlineData("ahuup95@yahoo.com")]
    [InlineData("hey_ok@outlook.com")]
    [InlineData("hey*ok@outloo-k.com")]
    [InlineData("heyok@123test.com")]
    public void CreateEmailShouldReturnOkGivenValidEmail(string email)
    {
        var emailResult = Types.Email.create(email);
        
        Assert.True(emailResult.IsOk);
        Assert.Equal(email, Types.Email.evaluate(emailResult.ResultValue));
        
    }

}