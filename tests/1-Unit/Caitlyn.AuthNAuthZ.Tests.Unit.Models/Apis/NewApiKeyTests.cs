using Caitlyn.AuthNAuthZ.Domain.Models.Apis;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Apis;

public class NewApiKeyTests
{
    [Fact]
    public void CreateNewApiShouldReturnOkGivenValidValues()
    {
        var expectedDto = new NewApiKey.NewApiKeyDto("Test", Guid.NewGuid(), Guid.NewGuid());
        var createNewApiKeyResult = NewApiKey.create(expectedDto);
        
        Assert.True(createNewApiKeyResult.IsOk);
        Assert.Equal(expectedDto, NewApiKey.toDto(createNewApiKeyResult.ResultValue));
    }

    [Theory]
    [MemberData(nameof(GetInvalidApiValues))]
    public void CreateNewApiShouldReturnErrorGivenInvalidValues(string name, Guid value, Guid appUuid)
    {
        var dto = new NewApiKey.NewApiKeyDto(name, value, appUuid);
        var createNewApiKeyResult = NewApiKey.create(dto);
        
        Assert.True(createNewApiKeyResult.IsError);
        Assert.NotNull(createNewApiKeyResult.ErrorValue);
    }

    public static IEnumerable<object[]> GetInvalidApiValues()
    {
        yield return new object[] { string.Empty, Guid.Empty, Guid.Empty };
        yield return new object[] { string.Empty, Guid.Empty, Guid.NewGuid() };
        yield return new object[] { string.Empty, Guid.NewGuid(), Guid.Empty };
        yield return new object[] { Guid.NewGuid().ToString(), Guid.NewGuid(), Guid.Empty };
        yield return new object[] { string.Empty, Guid.NewGuid(), Guid.NewGuid() };
        yield return new object[] { Guid.NewGuid().ToString(), Guid.Empty, Guid.Empty };
    }
}