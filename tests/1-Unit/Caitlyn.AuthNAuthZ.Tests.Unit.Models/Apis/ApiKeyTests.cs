using Caitlyn.AuthNAuthZ.Domain.Models.Apis;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Apis;

public class ApiKeyTests
{
    [Fact]
    public void CreateApiKeyShouldReturnOkGivenValidValues()
    {
        var expectedDto = new ApiKey.ApiKeyDto("Test", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid());
        var createApiKeyResult = ApiKey.create(expectedDto);
        
        Assert.True(createApiKeyResult.IsOk);
        Assert.Equal(expectedDto, ApiKey.toDto(createApiKeyResult.ResultValue));
    }

    [Theory]
    [MemberData(nameof(GetInvalidApiData))]
    public void CreateApiKeyShouldReturnErrorGivenInvalidValues(string name, Guid value, Guid apiUuid, Guid uuid)
    {
        var dto = new ApiKey.ApiKeyDto(name, value, apiUuid, uuid);
        var createApiKeyResult = ApiKey.create(dto);
        
        Assert.True(createApiKeyResult.IsError);
        Assert.NotNull(createApiKeyResult.ErrorValue);
    }

    public static IEnumerable<object[]> GetInvalidApiData()
    {
        yield return new object[] { string.Empty, Guid.Empty, Guid.Empty, Guid.Empty };
        yield return new object[] { string.Empty, Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };
        yield return new object[] { Guid.NewGuid().ToString(), Guid.Empty, Guid.NewGuid(), Guid.NewGuid() };
        yield return new object[] { Guid.NewGuid().ToString(), Guid.NewGuid(), Guid.Empty, Guid.NewGuid() };
        yield return new object[] { Guid.NewGuid().ToString(), Guid.NewGuid(), Guid.NewGuid(), Guid.Empty };
    }
}