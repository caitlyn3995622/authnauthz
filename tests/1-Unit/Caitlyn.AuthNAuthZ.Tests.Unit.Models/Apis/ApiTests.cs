using Caitlyn.AuthNAuthZ.Domain.Models.Apis;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Apis;

public class ApiTests
{
    [Fact]
    public void CreateApiShouldReturnOkGivenValidValues()
    {
        var expectedDto = new Api.ApiDto("Test", Guid.NewGuid());
        var createApiResult = Domain.Models.Apis.Api.create(expectedDto);
        
        Assert.True(createApiResult.IsOk);
        Assert.Equal(expectedDto, Domain.Models.Apis.Api.toDto(createApiResult.ResultValue));
    }

    [Theory]
    [MemberData(nameof(GetInvalidApiValues))]
    public void CreateApiShouldReturnErrorGivenInvalidValues(string name, Guid uuid)
    {
        var dto = new Api.ApiDto(name, uuid);
        var createApiResult = Domain.Models.Apis.Api.create(dto);
        
        Assert.True(createApiResult.IsError);
        Assert.NotNull(createApiResult.ErrorValue);
    }

    public static IEnumerable<object[]> GetInvalidApiValues()
    {
        yield return new object[] { string.Empty, Guid.Empty };
        yield return new object[] { string.Empty, Guid.NewGuid().ToString() };
        yield return new object[] { "Test", Guid.Empty };
    }
}