using Caitlyn.AuthNAuthZ.Domain.Models;
using Caitlyn.AuthNAuthZ.Domain.Models.Apis;
using Caitlyn.AuthNAuthZ.Domain.Models.Errors;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Apis;

public class ApiKeyValueTests
{
    [Theory]
    [MemberData(nameof(InvalidApiKeyValues))]
    public void CreateApiKeyValueShouldReturnErrorGivenInvalidGuid(Guid invalidUuid)
    {
        var apiKeyResult = ApiKeyValue.create(invalidUuid);
        
        Assert.True(apiKeyResult.IsError);
        Assert.True(apiKeyResult.ErrorValue.IsApiKeyError);
        var apiKeyError = (Errors.DomainErrors.ApiKeyError)apiKeyResult.ErrorValue;
        Assert.Equal(Errors.ApiKeyErrors.EmptyApiKey, apiKeyError.Item1);
        Assert.Equal(Errors.ErrorType.InvalidRequest, apiKeyError.Item2);

    }
    
    public static IEnumerable<object[]> InvalidApiKeyValues()
    {
        yield return new object[] { Guid.Empty };
    }

    [Fact]
    public void CreateApiKeyValueShouldReturnOkGivenValidGuid()
    {
        var guid = Guid.NewGuid();
        var apiKeyResult = ApiKeyValue.create(guid);
        
        Assert.True(apiKeyResult.IsOk);
        Assert.Equal(guid, ApiKeyValue.evaluate(apiKeyResult.ResultValue));
    }
}