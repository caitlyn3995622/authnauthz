using Caitlyn.AuthNAuthZ.Domain.Models;
using Caitlyn.AuthNAuthZ.Domain.Models.Apis;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Apis;

public class NewApiTests
{
    [Fact]
    public void CreateNewApiShouldReturnOkGivenValidValues()
    {
        var expectedDto = new NewApi.NewApiDto("Test");
        var createNewApiResult = Domain.Models.Apis.NewApi.create(expectedDto);
        
        Assert.True(createNewApiResult.IsOk);
        Assert.Equal(expectedDto, Domain.Models.Apis.NewApi.toDto(createNewApiResult.ResultValue));
    }

    [Theory]
    [MemberData(nameof(GetInvalidApiData))]
    public void CreateNewApiShouldReturnErrorGivenInvalidValues(string name)
    {
        var dto = new NewApi.NewApiDto(name);
        var createNewApiResult = Domain.Models.Apis.NewApi.create(dto);

        Assert.True(createNewApiResult.IsError);
        Assert.NotNull(createNewApiResult.ErrorValue);
    }

    public static IEnumerable<object[]> GetInvalidApiData()
    {
        yield return new object[] { string.Empty };
    }
}