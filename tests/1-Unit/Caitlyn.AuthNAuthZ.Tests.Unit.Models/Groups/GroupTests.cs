using Caitlyn.AuthNAuthZ.Domain.Models.Groups;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Groups;

public class GroupTests
{
    [Fact]
    public void CreateGroupShouldReturnOkGivenValidValues()
    {
        var expectedDto = new Group.GroupDto("Test", Guid.NewGuid());
        var createGroupResult = Domain.Models.Groups.Group.create(expectedDto);
        
        Assert.True(createGroupResult.IsOk);
        Assert.Equal(expectedDto, Domain.Models.Groups.Group.toDto(createGroupResult.ResultValue));
    }

    [Theory]
    [MemberData(nameof(GetInvalidData))]
    public void CreateGroupShouldReturnErrorGivenInvalidValues(string name, Guid uuid)
    {
        var dto = new Group.GroupDto(name, uuid);
        var createGroupResult = Domain.Models.Groups.Group.create(dto);

        Assert.True(createGroupResult.IsError);
        Assert.NotNull(createGroupResult.ErrorValue);
    }

    public static IEnumerable<object[]> GetInvalidData()
    {
        yield return new object[] { string.Empty, Guid.Empty };
        yield return new object[] { Guid.NewGuid().ToString(), Guid.Empty };
        yield return new object[] { string.Empty, Guid.NewGuid()};
    }

}