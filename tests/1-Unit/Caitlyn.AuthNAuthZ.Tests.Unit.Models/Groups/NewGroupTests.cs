using Caitlyn.AuthNAuthZ.Domain.Models.Groups;

namespace Caitlyn.AuthNAuthZ.Tests.Unit.Models.Groups;

public class NewGroupTests
{
    [Fact]
    public void CreateNewGroupShouldReturnOkGivenValidValues()
    {
        var expectedDto = new NewGroup.NewGroupDto("Test");
        var createNewGroupResult = Domain.Models.Groups.NewGroup.create(expectedDto);
        
        Assert.True(createNewGroupResult.IsOk);
        Assert.Equal(expectedDto, Domain.Models.Groups.NewGroup.toDto(createNewGroupResult.ResultValue));
    }
    
    [Theory]
    [MemberData(nameof(GetInvalidData))]
    public void CreateNewGroupShouldReturnErrorGivenInvalidValues(string name)
    {
        var dto = new NewGroup.NewGroupDto(name);
        var createNewGroupResult = Domain.Models.Groups.NewGroup.create(dto);

        Assert.True(createNewGroupResult.IsError);
        Assert.NotNull(createNewGroupResult.ErrorValue);
    }

    public static IEnumerable<object[]> GetInvalidData()
    {
        yield return new object[] { string.Empty };
        yield return new object[] { null };
        yield return new object[] { "           " };
    }
}