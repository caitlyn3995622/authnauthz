using Caitlyn.AuthNAuthZ.Domain.Cqrs.Users;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.DependencyInjectionTestsBase;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace Caitlyn.AuthNAuthZ.Tests.Component.Cqrs;

public class ComponentTestsBase : DependencyInjectionTestBase
{
    protected ComponentTestsBase() : base(ConfigureServices)
    {
        
    }
    
    public static void ConfigureServices(IServiceCollection serviceCollection)
    {
        Domain.Cqrs.ServiceCollectionExtensions.AddCqrs(serviceCollection);
        serviceCollection
            .AddScoped<Mock<IUserRepository>>(_ => new Mock<IUserRepository>())
            .AddScoped<IUserRepository>(sp => sp.GetRequiredService<Mock<IUserRepository>>().Object);
    }
}