using Caitlyn.AuthNAuthZ.Domain.Cqrs.Users;
using Caitlyn.AuthNAuthZ.Domain.Models.Errors;
using Caitlyn.AuthNAuthZ.Domain.Models.Users;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.DependencyInjectionTestsBase;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Asserts;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers.Users;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.FSharp.Core;
using Moq;

namespace Caitlyn.AuthNAuthZ.Tests.Component.Cqrs.Users;

public class AddUserCommandTests : ComponentTestsBase
{
    [Fact]
    public async Task ExecuteShouldReturnOkGivenValidValues()
    {
        const string userName = "Test";
        const string email = "email@test.com";
        const string password = "Test*123456789";
        var mockedAddedUser = new User.UserDto(Guid.NewGuid(), userName, email);

        using var scope = CreateScope();

        var mockedUserRepository = scope.ServiceProvider.GetRequiredService<Mock<IUserRepository>>();
        mockedUserRepository.Setup(x => x.CheckIfUserNameAlreadyInUseAsync(It.IsAny<string>()))
            .ReturnsAsync(false);

        mockedUserRepository.Setup(x => x.AddAsync(It.IsAny<NewUser.NewUser>()))
            .ReturnsAsync(mockedAddedUser);

        var sut = scope.ServiceProvider.GetRequiredService<IAddUserCommand>();
        var newUserDto = new NewUser.NewUserDto(userName, email, password);
        var result = sut.Execute(newUserDto);

        var newUserEqualityComparer = scope.ServiceProvider.GetRequiredService<INewUserEqualityComparer>();
        var resultAssert = scope.ServiceProvider.GetRequiredService<ResultAssert>();
        
        resultAssert.IsOk(mockedAddedUser, result);
        mockedUserRepository.Verify(x => x.AddAsync(It.Is<NewUser.NewUser>(y => newUserEqualityComparer.EqualsDto(y, newUserDto))), Times.Once);
    }

    [Fact]
    public async Task ExecuteShouldReturnUserNameAlreadyInUseErrorGivenUserNameAlreadyInUse()
    {
        using var scope = CreateScope();

        var mockedUserRepository = scope.ServiceProvider.GetRequiredService<Mock<IUserRepository>>();
        mockedUserRepository.Setup(x => x.CheckIfUserNameAlreadyInUseAsync(It.IsAny<string>()))
            .ReturnsAsync(true);
        

        var sut = scope.ServiceProvider.GetRequiredService<IAddUserCommand>();
        var result = sut.Execute(new NewUser.NewUserDto(Guid.NewGuid().ToString(), "test@email.com", "Test*123456789"));

        var resultAssert = scope.ServiceProvider.GetRequiredService<ResultAssert>();
        resultAssert.IsError(Errors.DomainErrors.NewUserError(Errors.UserErrors.UserNameAlreadyInUse, Errors.ErrorType.InvalidRequest), result);
        
        mockedUserRepository.Verify(x => x.AddAsync(It.IsAny<NewUser.NewUser>()), Times.Never);
    }

    [Fact]
    public async Task ExecuteShouldReturnErrorGivenInvalidUser()
    {
        using var scope = CreateScope();

        var mockedUserRepository = scope.ServiceProvider.GetRequiredService<Mock<IUserRepository>>();

        var sut = scope.ServiceProvider.GetRequiredService<IAddUserCommand>();
        var result = sut.Execute(new NewUser.NewUserDto(Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), "Test*123456789"));

        var resultAssert = scope.ServiceProvider.GetRequiredService<ResultAssert>();
        resultAssert.IsError(Errors.DomainErrors.NewTypeError(Errors.TypeErrors.InvalidEmail, Errors.ErrorType.InvalidRequest), result);
        
        mockedUserRepository.Verify(x => x.AddAsync(It.IsAny<NewUser.NewUser>()), Times.Never);
        mockedUserRepository.Verify(x => x.CheckIfUserNameAlreadyInUseAsync(It.IsAny<string>()), Times.Never);
    }
}