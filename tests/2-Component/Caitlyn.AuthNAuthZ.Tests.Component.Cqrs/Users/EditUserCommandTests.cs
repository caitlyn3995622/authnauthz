using Caitlyn.AuthNAuthZ.Domain.Cqrs.Users;
using Caitlyn.AuthNAuthZ.Domain.Models;
using Caitlyn.AuthNAuthZ.Domain.Models.Errors;
using Caitlyn.AuthNAuthZ.Domain.Models.Users;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Asserts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.FSharp.Core;
using Moq;

namespace Caitlyn.AuthNAuthZ.Tests.Component.Cqrs.Users;

public class EditUserCommandTests: ComponentTestsBase
{
    [Fact]
    public async Task ExecuteShouldEditUserProfileGivenValidRequest()
    {
        using var scope = CreateScope();

        const string newEmail = "editedEmail@test.com";
        var testId = Guid.NewGuid();
        var oldUserDto = new User.UserDto(testId, "testUser", "oldEmail@test.com");
        var newUserDto = new User.UserDto(testId, "testUser", newEmail);
        var inputDto = new EditUserProfile.EditUserProfileDto(newEmail);
        
        var mockedUserRepository = scope.ServiceProvider.GetRequiredService<Mock<IUserRepository>>();
        mockedUserRepository.Setup(x => x.GetAsync(It.Is<Types.Uuid.Uuid>(y => Types.Uuid.evaluate(y) == testId)))
            .ReturnsAsync(FSharpOption<User.UserDto>.Some(oldUserDto));
        mockedUserRepository.Setup(x => x.EditProfileAsync(
                It.Is<Types.Uuid.Uuid>(y => Types.Uuid.evaluate(y) == testId),
                It.Is<EditUserProfile.EditUserProfile>(y => EditUserProfile.evaluateEmail(y) == newEmail)))
            .ReturnsAsync(FSharpResult<User.UserDto, Errors.DomainErrors>.NewOk(newUserDto));

        var sut = scope.ServiceProvider.GetRequiredService<IEditUserProfileCommand>();
        var result = sut.Execute(testId.ToString(), inputDto);

        var resultAssert = scope.ServiceProvider.GetRequiredService<ResultAssert>();
        resultAssert.IsOk(newUserDto, result);
        mockedUserRepository.Verify(x => x.GetAsync(It.Is<Types.Uuid.Uuid>(y => Types.Uuid.evaluate(y) == testId)), Times.Once);
        mockedUserRepository.Verify(x => x.EditProfileAsync(
            It.Is<Types.Uuid.Uuid>(y => Types.Uuid.evaluate(y) == testId),
            It.Is<EditUserProfile.EditUserProfile>(y => EditUserProfile.evaluateEmail(y) == newEmail)), Times.Once);
    }

    [Fact]
    public async Task ExecuteShouldReturnErrorGivenInvalidRequest()
    {
        using var scope = CreateScope();
        
        var testId = Guid.NewGuid();
        var oldUserDto = new User.UserDto(testId, "testUser", "oldEmail@test.com");
        var inputDto = new EditUserProfile.EditUserProfileDto(Guid.NewGuid().ToString());
        
        var mockedUserRepository = scope.ServiceProvider.GetRequiredService<Mock<IUserRepository>>();
        mockedUserRepository.Setup(x => x.GetAsync(It.Is<Types.Uuid.Uuid>(y => Types.Uuid.evaluate(y) == testId)))
            .ReturnsAsync(FSharpOption<User.UserDto>.Some(oldUserDto));

        var sut = scope.ServiceProvider.GetRequiredService<IEditUserProfileCommand>();
        var result = sut.Execute(testId.ToString(), inputDto);

        var resultAssert = scope.ServiceProvider.GetRequiredService<ResultAssert>();
        resultAssert.IsError(Errors.DomainErrors.NewTypeError(Errors.TypeErrors.InvalidEmail, Errors.ErrorType.InvalidRequest), result);
        
        mockedUserRepository.Verify(x => x.EditProfileAsync(It.IsAny<Types.Uuid.Uuid>(), It.IsAny<EditUserProfile.EditUserProfile>()), Times.Never);
    }

    [Fact]
    public async Task ExecuteShouldReturnNotFoundGivenNonExistingUser()
    {
        using var scope = CreateScope();
        
        const string newEmail = "editedEmail@test.com";
        var testId = Guid.NewGuid();
        var inputDto = new EditUserProfile.EditUserProfileDto(newEmail);

        var mockedUserRepository = scope.ServiceProvider.GetRequiredService<Mock<IUserRepository>>();
        mockedUserRepository.Setup(x => x.GetAsync(It.Is<Types.Uuid.Uuid>(y => Types.Uuid.evaluate(y) == testId)))
            .ReturnsAsync(FSharpOption<User.UserDto>.None);
        
        var sut = scope.ServiceProvider.GetRequiredService<IEditUserProfileCommand>();
        var result = sut.Execute(testId.ToString(), inputDto);

        var resultAssert = scope.ServiceProvider.GetRequiredService<ResultAssert>();
        resultAssert.IsError(Errors.DomainErrors.NewUserError(Errors.UserErrors.UserNotFound, Errors.ErrorType.NotFound), result);
        
        mockedUserRepository.Verify(x => x.EditProfileAsync(It.IsAny<Types.Uuid.Uuid>(), It.IsAny<EditUserProfile.EditUserProfile>()), Times.Never);
    }
    
}