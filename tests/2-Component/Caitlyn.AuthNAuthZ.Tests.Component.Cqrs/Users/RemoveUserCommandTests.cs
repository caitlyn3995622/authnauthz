using Caitlyn.AuthNAuthZ.Domain.Cqrs.Users;
using Caitlyn.AuthNAuthZ.Domain.Models;
using Caitlyn.AuthNAuthZ.Domain.Models.Errors;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.FSharp.Core;
using Moq;

namespace Caitlyn.AuthNAuthZ.Tests.Component.Cqrs.Users;

public class RemoveUserCommandTests: ComponentTestsBase
{
    [Fact]
    public async Task ExecuteShouldDeleteUserGivenValidUuid()
    {
        using var scope = CreateScope();

        var mockedRepository = scope.ServiceProvider.GetRequiredService<Mock<IUserRepository>>();
        mockedRepository
            .Setup(x => x.RemoveAsync(It.IsAny<Types.Uuid.Uuid>()))
            .ReturnsAsync(FSharpOption<Errors.DomainErrors>.None);

        var sut = scope.ServiceProvider.GetRequiredService<IRemoveUserCommand>();
        var uuid = Guid.NewGuid();
        var errors = sut.Execute(uuid.ToString());
        
        Assert.True(FSharpOption<Errors.DomainErrors>.get_IsNone(errors));
        mockedRepository.Verify(x => x.RemoveAsync(It.Is<Types.Uuid.Uuid>(y => Types.Uuid.evaluate(y) == uuid)), Times.Once);
        mockedRepository.Verify(x => x.RemoveAsync(It.Is<Types.Uuid.Uuid>(y => Types.Uuid.evaluate(y) != uuid)), Times.Never);
    }

    [Fact]
    public async Task ExecuteShouldReturnInvalidUuidGivenInvalidUuid()
    {
        var expectedError = Errors.DomainErrors.NewTypeError(Errors.TypeErrors.InvalidUuid, Errors.ErrorType.InvalidRequest);
        using var scope = CreateScope();
        
        var mockedRepository = scope.ServiceProvider.GetRequiredService<Mock<IUserRepository>>();
        mockedRepository
            .Setup(x => x.RemoveAsync(It.IsAny<Types.Uuid.Uuid>()))
            .ReturnsAsync(FSharpOption<Errors.DomainErrors>.Some(expectedError));

        var sut = scope.ServiceProvider.GetRequiredService<IRemoveUserCommand>();
        var error = sut.Execute("crazy-uuid-should-fail");
        
        Assert.True(FSharpOption<Errors.DomainErrors>.get_IsSome(error));
        var errorValue = error.Value;
        Assert.Equal(expectedError, errorValue);
        
        mockedRepository.Verify(x => x.RemoveAsync(It.IsAny<Types.Uuid.Uuid>()), Times.Never);
    }

    [Fact]
    public async Task ExecuteShouldReturnUserNotFoundGivenNonExistingUuid()
    {
        var expectedError = Errors.DomainErrors.NewUserError(Errors.UserErrors.UserNotFound, Errors.ErrorType.NotFound);
        using var scope = CreateScope();
        
        var mockedRepository = scope.ServiceProvider.GetRequiredService<Mock<IUserRepository>>();
        mockedRepository
            .Setup(x => x.RemoveAsync(It.IsAny<Types.Uuid.Uuid>()))
            .ReturnsAsync(FSharpOption<Errors.DomainErrors>.Some(expectedError));

        var sut = scope.ServiceProvider.GetRequiredService<IRemoveUserCommand>();
        var uuid = Guid.NewGuid();
        var error = sut.Execute(uuid.ToString());
        
        Assert.True(FSharpOption<Errors.DomainErrors>.get_IsSome(error));
        var errorValue = error.Value;
        Assert.Equal(expectedError, errorValue);
        
        mockedRepository.Verify(x => x.RemoveAsync(It.Is<Types.Uuid.Uuid>(y => Types.Uuid.evaluate(y) == uuid)), Times.Once);        
    }
}