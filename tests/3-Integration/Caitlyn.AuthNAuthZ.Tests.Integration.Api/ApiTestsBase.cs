using System.Text;
using System.Text.Json;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.DatabaseOperations;

namespace Caitlyn.AuthNAuthZ.Tests.Integration.Api;

[Collection(AuthNAuthZApplicationFactoryCollectionDefinition.CollectionName)]
public class ApiTestsBase: IDisposable, IAsyncDisposable
{
    private readonly AuthNAuthZApplicationFactory _applicationFactory;
    
    public IServiceScopeFactory ScopeFactory { get; set; }

    protected ApiTestsBase(AuthNAuthZApplicationFactory applicationFactory)
    {
        _applicationFactory = applicationFactory;
        _ = _applicationFactory.CreateClient();

        ScopeFactory = _applicationFactory.Services.GetRequiredService<IServiceScopeFactory>();
    }

    protected IServiceScope CreateScope() => ScopeFactory.CreateScope();
    protected HttpClient CreateClient() => _applicationFactory.CreateClient();

    protected StringContent GetJson(object payload)
    {
        using var scope = CreateScope();
        var options = scope.ServiceProvider.GetRequiredService<JsonSerializerOptions>();
        var serializedPayload = JsonSerializer.Serialize(payload, options);

        return new StringContent(serializedPayload, Encoding.UTF8, "application/json");
    }
    
    public void Dispose() => DisposeAsync().GetAwaiter().GetResult();
    public async ValueTask DisposeAsync()
    {
        using var scope = CreateScope();
        var cleaner = scope.ServiceProvider.GetRequiredService<DatabaseCleaner>();
        await cleaner.CleanAsync();
    }
}