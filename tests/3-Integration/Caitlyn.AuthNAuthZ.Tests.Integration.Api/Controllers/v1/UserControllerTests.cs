using System.Net;
using Caitlyn.AuthNAuthZ.Domain.Models.Users;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.DatabaseOperations;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.HttpOperations;
using Newtonsoft.Json;

namespace Caitlyn.AuthNAuthZ.Tests.Integration.Api.Controllers.v1;

public class UserControllerTests : ApiTestsBase
{
    public UserControllerTests(AuthNAuthZApplicationFactory applicationFactory) : base(applicationFactory)
    {
    }

    [Fact]
    public async Task PostShouldCreateUserGivenValidRequest()
    {
        using var client = CreateClient();
        using var scope = CreateScope();
        var dto = new NewUser.NewUserDto("Test", "ok@test.com", "Test*123456789");

        var response = await client.PostAsync("api/v1/user", GetJson(dto));

        var httpAssert = scope.ServiceProvider.GetRequiredService<HttpAssert>();

        await httpAssert.True<User.UserDto>(HttpStatusCode.Created, response, async (sp, result) =>
        {
            Assert.True(result.IsOk);
            var dto = result.Result!;

            var databaseAssert = sp.GetRequiredService<DatabaseAssert>();
            Assert.True(await databaseAssert.UserWithUuidExistsAsync(dto!.Uuid));
        });
    }

    [Fact]
    public async Task PostShouldReturnErrorsGivenInvalidRequest()
    {
        using var client = CreateClient();
        using var scope = CreateScope();
        var dto = new NewUser.NewUserDto("Test", Guid.NewGuid().ToString(), Guid.NewGuid().ToString());

        var response = await client.PostAsync("api/v1/user", GetJson(dto));

        var httpAssert = scope.ServiceProvider.GetRequiredService<HttpAssert>();

        await httpAssert.True(HttpStatusCode.BadRequest, response, async (_, result) => { Assert.False(result.IsOk); });
    }

    [Fact]
    public async Task PostShouldReturnErrorGivenUserNameAlreadyRegisteredInDatabase()
    {
        using var client = CreateClient();
        using var scope = CreateScope();
        
        var generator = scope.ServiceProvider.GetRequiredService<DatabaseGenerator>();
        var generatedUser = await generator.GenerateUser();
        
        var dto = new NewUser.NewUserDto(generatedUser.UserName, "test@test.com", "Test*123456789");

        var response = await client.PostAsync("api/v1/user", GetJson(dto));

        var httpAssert = scope.ServiceProvider.GetRequiredService<HttpAssert>();

        await httpAssert.True(HttpStatusCode.BadRequest, response, async (_, result) =>
        {
            Assert.False(result.IsOk);
            Assert.Single(result.Errors);
            Assert.Equal("UserNameAlreadyInUse", result.Errors.First());
        });

    }

    [Fact]
    public async Task PostShouldReturnErrorGivenEmailAlreadyRegisteredInDatabase()
    {
        using var client = CreateClient();
        using var scope = CreateScope();
        
        var generator = scope.ServiceProvider.GetRequiredService<DatabaseGenerator>();
        var generatedUser = await generator.GenerateUser();
        
        var dto = new NewUser.NewUserDto("Ahuupa", generatedUser.Email, "Test*123456789");

        var response = await client.PostAsync("api/v1/user", GetJson(dto));

        var httpAssert = scope.ServiceProvider.GetRequiredService<HttpAssert>();
        await httpAssert.True(HttpStatusCode.BadRequest, response, async (_, result) =>
        {
            Assert.False(result.IsOk);
            Assert.Single(result.Errors);
            Assert.Equal("EmailAlreadyExists", result.Errors.First());
        });    
    }

    [Fact]
    public async Task GetShouldReturn404GivenNonExistingUser()
    {
        using var client = CreateClient();
        using var scope = CreateScope();
        
        var response = await client.GetAsync($"api/v1/user/{Guid.NewGuid()}");
        
        var httpAssert = scope.ServiceProvider.GetRequiredService<HttpAssert>();
        await httpAssert.True(HttpStatusCode.NotFound, response, async (_, result) =>
        {
            Assert.False(result.IsOk);
            Assert.Single(result.Errors);
            Assert.Equal("UserNotFound", result.Errors.First());
        });
    }
    
    [Fact]
    public async Task GetShouldReturnInvalidUuidGivenInvalidUuid()
    {
        using var client = CreateClient();
        using var scope = CreateScope();
        
        var response = await client.GetAsync($"api/v1/user/crazy-uuid");
        
        var httpAssert = scope.ServiceProvider.GetRequiredService<HttpAssert>();
        await httpAssert.True(HttpStatusCode.BadRequest, response, async (_, result) =>
        {
            Assert.False(result.IsOk);
            Assert.Single(result.Errors);
            Assert.Equal("InvalidUuid", result.Errors.First());
        });
    }

    [Fact]
    public async Task GetShouldReturnUserGivenExistingUser()
    {
        using var client = CreateClient();
        using var scope = CreateScope();
        
        var generator = scope.ServiceProvider.GetRequiredService<DatabaseGenerator>();
        var generatedUser = await generator.GenerateUser();
 
        var response = await client.GetAsync($"api/v1/user/{generatedUser.Uuid}");
        
        var httpAssert = scope.ServiceProvider.GetRequiredService<HttpAssert>();
        await httpAssert.True<User.UserDto>(HttpStatusCode.OK, response, async (_, result) =>
        {
            Assert.True(result.IsOk);
            Assert.Equal(generatedUser.Uuid, result.Result!.Uuid);
            Assert.Equal(generatedUser.UserName, result.Result!.UserName);
            Assert.Equal(generatedUser.Email, result.Result!.Email);
        });
    }

    [Fact]
    public async Task DeleteShouldDeleteUserFromDatabaseGivenExistingUuid()
    {
        using var client = CreateClient();
        using var scope = CreateScope();
        
        var generator = scope.ServiceProvider.GetRequiredService<DatabaseGenerator>();
        var userToRemain = await generator.GenerateUser();
        var userToDelete = await generator.GenerateUser();

        var response = await client.DeleteAsync($"api/v1/user/{userToDelete.Uuid}");
        
        var httpAssert = scope.ServiceProvider.GetRequiredService<HttpAssert>();
        await httpAssert.True(HttpStatusCode.OK, response, async (_, result) =>
        {
            Assert.True(result.IsOk);
        });

        var databaseAssert = scope.ServiceProvider.GetRequiredService<DatabaseAssert>();
        Assert.True(await databaseAssert.UserWithUuidExistsAsync(userToRemain.Uuid));
        Assert.False(await databaseAssert.UserWithUuidExistsAsync(userToDelete.Uuid));
    }

    [Fact]
    public async Task DeleteShouldReturn404GivenNonExistingUuid()
    {
        using var client = CreateClient();
        using var scope = CreateScope();
        
        var response = await client.GetAsync($"api/v1/user/{Guid.NewGuid()}");
        
        var httpAssert = scope.ServiceProvider.GetRequiredService<HttpAssert>();
        await httpAssert.True(HttpStatusCode.NotFound, response, async (_, result) =>
        {
            Assert.False(result.IsOk);
            Assert.Single(result.Errors);
            Assert.Equal("UserNotFound", result.Errors.First());
        });
    }

    [Fact]
    public async Task PutShouldUpdateUserGivenValidRequest()
    {
        using var client = CreateClient();
        using var scope = CreateScope();

        const string newEmail = "editedEmail@test.com";
        var generator = scope.ServiceProvider.GetRequiredService<DatabaseGenerator>();
        var userToRemain = await generator.GenerateUser();
        var userToEdit = await generator.GenerateUser();

        var request = new EditUserProfile.EditUserProfileDto(newEmail);
        var response = await client.PutAsync($"api/v1/user/{userToEdit.Uuid}", GetJson(request));

        var httpAssert = scope.ServiceProvider.GetRequiredService<HttpAssert>();
        await httpAssert.True<User.UserDto>(HttpStatusCode.OK, response, async (sp, result) =>
        {
            Assert.True(result.IsOk);
            Assert.Equal(newEmail, result.Result!.Email);

            var databaseAssert = sp.GetRequiredService<DatabaseAssert>();
            await databaseAssert.UserMatches(result.Result.Uuid, y => Assert.Equal(newEmail, y.Email));
        });
        var databaseAssert = scope.ServiceProvider.GetRequiredService<DatabaseAssert>();
        await databaseAssert.UserMatches(userToRemain.Uuid, y => Assert.Equal(userToRemain.Email, y.Email));
    }

    [Fact]
    public async Task PutShouldReturn404GivenNonExistingUser()
    {
        using var client = CreateClient();
        using var scope = CreateScope();
        
        const string newEmail = "editedEmail@test.com";
        var request = new EditUserProfile.EditUserProfileDto(newEmail);

        var response = await client.PutAsync($"api/v1/user/{Guid.NewGuid()}", GetJson(request));
        var httpAssert = scope.ServiceProvider.GetRequiredService<HttpAssert>();
        await httpAssert.True(HttpStatusCode.NotFound, response, async (_, result) =>
        {
            Assert.False(result.IsOk);
            Assert.Single(result.Errors);
            Assert.Equal("UserNotFound", result.Errors.First());
        });
    }
}