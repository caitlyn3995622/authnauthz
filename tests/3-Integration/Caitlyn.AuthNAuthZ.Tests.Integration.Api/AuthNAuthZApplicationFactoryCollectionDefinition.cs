namespace Caitlyn.AuthNAuthZ.Tests.Integration.Api;

[CollectionDefinition(CollectionName)]
public class AuthNAuthZApplicationFactoryCollectionDefinition: ICollectionFixture<AuthNAuthZApplicationFactory>
{
    public const string CollectionName = "AuthNAuthZApplicationFactory";
}