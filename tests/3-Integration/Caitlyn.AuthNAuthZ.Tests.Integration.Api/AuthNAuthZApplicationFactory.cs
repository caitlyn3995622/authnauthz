using Caitlyn.AuthNAuthZ.CrossCutting.Extensions;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.DatabaseOperations;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Asserts;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers.Users;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.HttpOperations;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Neo4j.Driver;

namespace Caitlyn.AuthNAuthZ.Tests.Integration.Api;

public class AuthNAuthZApplicationFactory : WebApplicationFactory<Program>
{
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        base.ConfigureWebHost(builder);

        builder.ConfigureAppConfiguration((_, config) =>
        {
            config
                .SetBasePath(GetType().Assembly.GetDirectoryName())
                .AddJsonFile("appsettings.tests.json");
        });

        builder.ConfigureTestServices(sp =>
        {
            sp
                .AddSingleton<DatabaseAssert>()
                .AddSingleton<DatabaseCleaner>()
                .AddSingleton<DatabaseGenerator>()
                .AddSingleton<ResultAssert>()
                .AddSingleton<INewUserEqualityComparer, NewUserEqualityComparer>()
                .AddSingleton<IUserEqualityComparer, UserEqualityComparer>()
                .AddSingleton<HttpAssert>();
        });
    }
}