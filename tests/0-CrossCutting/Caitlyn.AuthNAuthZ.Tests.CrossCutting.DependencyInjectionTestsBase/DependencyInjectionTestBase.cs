using Microsoft.Extensions.DependencyInjection;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion;
using Caitlyn.AuthNAuthZ.Tests.CrossCutting.DatabaseOperations;

namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.DependencyInjectionTestsBase;

public class DependencyInjectionTestBase: IDisposable, IAsyncDisposable
{
    private readonly ServiceProvider _serviceProvider;

    public IServiceScopeFactory ServiceScopeFactory { get; }
    
    protected DependencyInjectionTestBase(Action<IServiceCollection>? customServicesCallback = null)
    {
        var serviceCollection = new ServiceCollection();

        FSharpAssertion.ServiceCollectionExtensions.AddCustomAssertions(serviceCollection);
        serviceCollection.AddDatabaseOperations();

        customServicesCallback?.Invoke(serviceCollection);
        _serviceProvider = serviceCollection.BuildServiceProvider();
        ServiceScopeFactory = _serviceProvider.GetRequiredService<IServiceScopeFactory>();
    }

    public IServiceScope CreateScope() => ServiceScopeFactory.CreateScope();

    public void Dispose() => DisposeAsync().GetAwaiter().GetResult();

    public virtual async ValueTask DisposeAsync()
    {
        await _serviceProvider.DisposeAsync();
    }
}