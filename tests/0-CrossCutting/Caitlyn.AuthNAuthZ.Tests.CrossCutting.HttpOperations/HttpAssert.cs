﻿using System.Net;
using System.Text.Json;
using Caitlyn.AuthNAuthZ.Presentation.Api;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.HttpOperations;

public class HttpAssert
{
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public HttpAssert(IServiceScopeFactory serviceScopeFactory)
    {
        _serviceScopeFactory = serviceScopeFactory;
    }


    public async Task True(HttpStatusCode expectedStatusCode, HttpResponseMessage responseMessage, Func<IServiceProvider, ApiResult, Task> apiResultCallback)
    {
        using var scope = _serviceScopeFactory.CreateScope();
        var jsonOptions = scope.ServiceProvider.GetRequiredService<JsonSerializerOptions>();
        
        var serializedResponse = await responseMessage.Content.ReadAsStringAsync();
        Assert.Equal(expectedStatusCode, responseMessage.StatusCode);
        
        var deserializedResponse = JsonSerializer.Deserialize<ApiResult>(serializedResponse, jsonOptions);
        
        Assert.NotNull(deserializedResponse);

        await apiResultCallback.Invoke(scope.ServiceProvider, deserializedResponse!);
    }
    
    public async Task True<TResult>(HttpStatusCode expectedStatusCode, HttpResponseMessage responseMessage, Func<IServiceProvider, ApiResult<TResult>, Task> apiResultCallback)
    {
        using var scope = _serviceScopeFactory.CreateScope();
        var jsonOptions = scope.ServiceProvider.GetRequiredService<JsonSerializerOptions>();
        
        var serializedResponse = await responseMessage.Content.ReadAsStringAsync();
        Assert.Equal(expectedStatusCode, responseMessage.StatusCode);
        
        var deserializedResponse = JsonSerializer.Deserialize<ApiResult<TResult>>(serializedResponse, jsonOptions);
        
        Assert.NotNull(deserializedResponse);
        Assert.NotNull(deserializedResponse!.Result);

        await apiResultCallback.Invoke(scope.ServiceProvider, deserializedResponse);
    }
}