using Caitlyn.AuthNAuthZ.Domain.Models.Users;
using Caitlyn.AuthNAuthZ.Infrastructure.Neo4j;
using Caitlyn.AuthNAuthZ.Infrastructure.Neo4j.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.DatabaseOperations;

public class DatabaseGenerator
{
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public DatabaseGenerator(IServiceScopeFactory serviceScopeFactory)
    {
        _serviceScopeFactory = serviceScopeFactory;
    }

    public async Task<User.UserDto> GenerateUser()
    {
        using var scope = _serviceScopeFactory.CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<Neo4jContext>();

        var node = new DatabaseUser
        {
            Uuid = Guid.NewGuid(),
            Email = $"{Guid.NewGuid():N}@test.com",
            PasswordHash = Guid.NewGuid().ToString(),
            UserName = Guid.NewGuid().ToString()
        };
        await context.CreateNodeAsync(node);

        return new User.UserDto(node.Uuid, node.UserName, node.Email);
    }
}