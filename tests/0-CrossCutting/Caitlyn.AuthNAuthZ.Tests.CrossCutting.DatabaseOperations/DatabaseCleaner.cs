using Microsoft.Extensions.DependencyInjection;
using Neo4j.Driver;

namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.DatabaseOperations;

public class DatabaseCleaner
{
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public DatabaseCleaner(IServiceScopeFactory serviceScopeFactory)
    {
        _serviceScopeFactory = serviceScopeFactory;
    }

    public async Task CleanAsync()
    {
        using var scope = _serviceScopeFactory.CreateScope();
        var neo4jDriver = scope.ServiceProvider.GetRequiredService<IDriver>();
        await using var session = neo4jDriver.AsyncSession();
        await session.ExecuteWriteAsync(async queryRunner =>
        {
            await queryRunner.RunAsync("MATCH (n) DETACH DELETE n");
        });
    }
}