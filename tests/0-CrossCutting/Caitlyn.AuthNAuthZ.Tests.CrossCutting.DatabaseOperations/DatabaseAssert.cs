using Caitlyn.AuthNAuthZ.Domain.Cqrs.Users;
using Caitlyn.AuthNAuthZ.Domain.Models;
using Caitlyn.AuthNAuthZ.Domain.Models.Users;
using Caitlyn.AuthNAuthZ.Infrastructure.Neo4j.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.FSharp.Core;
using Xunit;

namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.DatabaseOperations;

internal class DatabaseAssert
{
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public DatabaseAssert(IServiceScopeFactory serviceScopeFactory)
    {
        _serviceScopeFactory = serviceScopeFactory;
    }

    public async Task<bool> UserWithUuidExistsAsync(Guid uuid)
    {
        var user = await GetUser(uuid);
        return FSharpOption<User.UserDto>.get_IsSome(user);
    }

    public async Task UserMatches(string uuid, Action<User.UserDto> userMatchesCallback) =>
        UserMatches(Guid.Parse(uuid), userMatchesCallback);

    public async Task UserMatches(Guid uuid, Action<User.UserDto> userMatchesCallback)
    {
        var user = await GetUser(uuid);
        Assert.True(FSharpOption<User.UserDto>.get_IsSome(user));
        
        userMatchesCallback.Invoke(user.Value);
    }

    private async Task<FSharpOption<User.UserDto>> GetUser(Guid uuid)
    {
        using var scope = _serviceScopeFactory.CreateScope();
        var repository = scope.ServiceProvider.GetRequiredService<IUserRepository>();
        var uuidResult = Types.Uuid.createFromGuid(uuid);
        Assert.True(uuidResult.IsOk);

        var user = await repository.GetAsync(uuidResult.ResultValue);
        return user;
    }
}