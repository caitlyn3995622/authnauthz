using Microsoft.Extensions.DependencyInjection;

namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.DatabaseOperations;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddDatabaseOperations(this IServiceCollection serviceCollection)
    {
        serviceCollection
            .AddSingleton<DatabaseAssert>()
            .AddSingleton<DatabaseCleaner>()
            .AddSingleton<DatabaseGenerator>();

        return serviceCollection;
    }
}