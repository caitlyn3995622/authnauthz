namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Asserts

open System.Collections.Generic
open Caitlyn.AuthNAuthZ.Domain.Models.Errors
open Caitlyn.AuthNAuthZ.Domain.Models.Errors.Errors
open Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers
open Xunit

type ResultAssert() =
    member this.IsError<'a> ((expectedError: DomainErrors), (result: Result<'a, DomainErrors>)) =
        match result with
            | Ok _ -> Assert.True false
            | Error error -> Assert.Equal (expectedError, error)
            
    member this.IsOkActualDomainToExpectedDto<'domainType, 'dtoType>(expectedDto: 'dtoType, result: Result<'domainType, DomainErrors>, comparer: IDomainAndDtoEqualityComparer<'domainType, 'dtoType>) =
        match result with
            | Error _ -> Assert.True false
            | Ok domain  ->
                comparer.EqualsDto domain expectedDto
                |> Assert.True
                
    member this.IsOk<'a> (expected: 'a, result : Result<'a, DomainErrors>) =
        match result with
            | Error _ -> Assert.True false
            | Ok a -> Assert.Equal(expected, a)