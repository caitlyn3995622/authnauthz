namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion

open Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Asserts
open Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers.Users
open Microsoft.Extensions.DependencyInjection

module ServiceCollectionExtensions =
    let AddCustomAssertions(services : IServiceCollection) =
        services
            .AddSingleton<IUserEqualityComparer, UserEqualityComparer>()
            .AddSingleton<INewUserEqualityComparer, NewUserEqualityComparer>()
            .AddSingleton<ResultAssert>()
