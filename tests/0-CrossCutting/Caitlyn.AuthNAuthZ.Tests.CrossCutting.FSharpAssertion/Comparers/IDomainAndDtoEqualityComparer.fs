namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers

type IDomainAndDtoEqualityComparer<'domainType, 'dtoType> = 
    abstract member EqualsDto: 'domainType -> 'dtoType -> bool
    abstract member GetHashCode: obj -> int
    

