namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers.Users

open Caitlyn.AuthNAuthZ.Domain.Models.Users.NewUser
open Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers

type INewUserEqualityComparer =
    inherit IDomainAndDtoEqualityComparer<NewUser, NewUserDto>