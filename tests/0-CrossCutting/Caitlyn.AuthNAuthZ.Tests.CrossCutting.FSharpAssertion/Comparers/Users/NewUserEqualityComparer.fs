namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers.Users

open Caitlyn.AuthNAuthZ.Domain.Models.Users
open Caitlyn.AuthNAuthZ.Domain.Models.Users.NewUser
open Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers

type NewUserEqualityComparer() =
    interface INewUserEqualityComparer with
        member this.EqualsDto newUser newUserDto =
            NewUser.getEmail newUser = newUserDto.Email
            && NewUser.getPassword newUser = newUserDto.Password
            && NewUser.getUserName newUser = newUserDto.UserName
        member this.GetHashCode(var) = var.GetHashCode()
        