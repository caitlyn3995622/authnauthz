namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers.Users

open Caitlyn.AuthNAuthZ.Domain.Models.Users
open Caitlyn.AuthNAuthZ.Domain.Models.Users.User
open Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers

type UserEqualityComparer() =
    interface IUserEqualityComparer with
        member this.EqualsDto user userDto =
            User.getEmail user = userDto.Email
            && User.getUuid user = userDto.Uuid
            && User.getUserName user = userDto.UserName

        member this.GetHashCode(user) = user.GetHashCode()