namespace Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers.Users

open Caitlyn.AuthNAuthZ.Domain.Models.Users.User
open Caitlyn.AuthNAuthZ.Tests.CrossCutting.FSharpAssertion.Comparers

type IUserEqualityComparer =
    inherit IDomainAndDtoEqualityComparer<User, UserDto>

